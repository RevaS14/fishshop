<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/categories', 'App\Http\Controllers\CategoryController@index');
Route::get('/products', 'App\Http\Controllers\ProductController@index');
Route::post('/login', 'App\Http\Controllers\CategoryController@login');
Route::get('/sliders', 'App\Http\Controllers\SliderController@index');
Route::get('/categories/{id}', 'App\Http\Controllers\CategoryController@show');
Route::get('/getSubMenu/{id}', 'App\Http\Controllers\CategoryController@getSubMenu');
Route::get('/getLocations', 'App\Http\Controllers\SiteController@getLocations');
Route::get('/category/{link}', 'App\Http\Controllers\ProductController@category');
Route::get('/product/{link}', 'App\Http\Controllers\ProductController@product');
Route::get('/userorders/{id}', 'App\Http\Controllers\SiteController@userorders');
Route::get('/vieworder/{id}', 'App\Http\Controllers\SiteController@vieworder');
Route::get('/getshipping/{id}', 'App\Http\Controllers\SiteController@getShipping');
Route::get('/getorder/{id}', 'App\Http\Controllers\SiteController@getOrder');
Route::get('/productImages/{link}', 'App\Http\Controllers\ProductController@productImages');
Route::get('/related/{id}', 'App\Http\Controllers\ProductController@related');
Route::post('/orders', 'App\Http\Controllers\SiteController@orders');
Route::post('/signup', 'App\Http\Controllers\SiteController@signup');
Route::post('/update-address', 'App\Http\Controllers\SiteController@updateAddress');
Route::get('/get-user/{id}', 'App\Http\Controllers\SiteController@getUser');
Route::post('/contact', 'App\Http\Controllers\SiteController@contact');
