<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\AdminController@index');
Route::post('/login', 'App\Http\Controllers\AdminController@login');
Route::get('/dashboard', 'App\Http\Controllers\AdminController@dashboard');
Route::get('/categories', 'App\Http\Controllers\AdminController@categories');
Route::get('/add-category', 'App\Http\Controllers\AdminController@addCategory');
Route::post('/manage-category', 'App\Http\Controllers\AdminController@manageCategory');
Route::get('/edit-category/{id}', 'App\Http\Controllers\AdminController@editCategory');
Route::post('/delete', 'App\Http\Controllers\AdminController@delete');

Route::get('/banners', 'App\Http\Controllers\AdminController@banners');
Route::get('/add-banner', 'App\Http\Controllers\AdminController@addBanner');
Route::post('/manage-banner', 'App\Http\Controllers\AdminController@manageBanner');
Route::get('/edit-banner/{id}', 'App\Http\Controllers\AdminController@editBanner');

Route::get('/products', 'App\Http\Controllers\AdminController@products');
Route::get('/add-product', 'App\Http\Controllers\AdminController@addProduct');
Route::post('/manage-product', 'App\Http\Controllers\AdminController@manageProduct');
Route::get('/edit-product/{id}', 'App\Http\Controllers\AdminController@editProduct');

Route::get('/locations', 'App\Http\Controllers\AdminController@locations');
Route::get('/add-location', 'App\Http\Controllers\AdminController@addLocation');
Route::post('/manage-location', 'App\Http\Controllers\AdminController@manageLocation');
Route::get('/edit-location/{id}', 'App\Http\Controllers\AdminController@editLocation');

Route::get('/users', 'App\Http\Controllers\AdminController@users');
Route::get('/orders', 'App\Http\Controllers\AdminController@orders');
Route::get('/view-order/{id}', 'App\Http\Controllers\AdminController@viewOrder');
Route::get('/print-invoice/{id}', 'App\Http\Controllers\AdminController@printInvoice');
Route::get('/complete-order/{id}', 'App\Http\Controllers\AdminController@completeOrder');
Route::get('/view-user/{id}', 'App\Http\Controllers\AdminController@viewUser');

