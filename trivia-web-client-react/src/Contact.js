import React, { Component } from 'react';
import { API_BASE_URL } from './config';
import { Redirect } from "react-router-dom";

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
        name: "",
        email: "",
        mobile: "",
        message: ""
      };
  } 

  handleChange = e => this.setState({ [e.target.name]: e.target.value, error: "" });

  contact = (e) => {
    e.preventDefault();

    const { name, email,mobile, message } = this.state;
    if (!name || !email || !mobile || !message) {
      return this.setState({ error: "Fill all fields!" });
    }
    
    this.props.contact(name, email,mobile, message)
    .then((loggedIn) => {
        return this.setState({ success: "Your Message has been sent..We will contact you soon.." });
    })
  };



  render() {
    return ( 
    <div>
       <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Contact Us</h1>					
                    </div>
                </div>
<div class="mad-content no-pd with-bg">
        <div class="container">
          <div class="mad-section pad-top-0 mad-section--stretched mad-colorizer--scheme-">
          <div class="bubbles bubble-1"></div>
          <div class="bubbles bubble-5"></div>
          <div class="bubbles bubble-2"></div>
          <div class="bubbles bubble-6"></div>
          <div class="bubbles bubble-3"></div>
          <div class="bubbles bubble-7"></div>
          <div class="bubbles bubble-4"></div>
          <div class="bubbles bubble-8"></div>
            <div class="mad-colorizer-bg-color">
              <div class="with-svg-item bottom2"><img src="assets/images/bottom_half_left.png" alt="" /></div>
            </div>
            <div class="row hr-size-2 pad-top-50">
              <div class="col-xl-8">
                <div class="row hr-size-2">
                  <div class="col-md-5">
                    <div class="mad-vr-list content-element-5">
                      <ul>
                      <li>
                      <h2 class="mb-10">Thatha Fish Shop</h2>
                      92,Edayarpalayam - Vadavalli Road,<br/>
                      Anna Nagar Housing Unit, <br/>
                      Edayarpalayam,<br/>
                      Coimbatore- 641025
                       </li>
                        <li>
                            +91 78100 64658 <br/>
                          <a href="#" class="mad-link">info@thathafishshop.com</a>
                        </li>
                      </ul>
                    </div>
                   
                  </div>
                  <div class="col-md-7">
                    {this.state.error && (
                          <div className="text-danger">{this.state.error}</div>
                    )}
                     {this.state.success && (
                          <div className="text-success">{this.state.success}</div>
                    )}
                    <p>If you have any further enquiries, please use the form below.</p>
                    <form class="mad-contact-form size-2" onSubmit={this.contact}>
                      <div class="mad-col">
                        <input type="text" id="cf_name" name="name" required="" onChange={this.handleChange} placeholder="Your Name"/>
                      </div>
                      <div class="mad-col">
                        <input type="email" id="cf_email" name="email" required="" onChange={this.handleChange} placeholder="E-mail"/>
                      </div>
                      <div class="mad-col">
                        <input type="number" id="cf_mobile" name="mobile" required="" onChange={this.handleChange} placeholder="Mobile"/>
                      </div>
                      <div class="mad-col">
                        <textarea rows="4" id="message" name="message" required="" onChange={this.handleChange} placeholder="Message"></textarea>
                      </div>
                      <div class="mad-col">
                        <button type="submit" class="btn"><span>Submit</span></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="col-xl-4">
                <div class="mad-tt-element no-border">
                  <span class="mad-tt-svg"><span><img src="assets/images/crab.png" alt=""/></span></span>
                  <h4 class="mad-title">We Are Open</h4>
                  <div class="mad-timetable mad-vr-list">
                  <ul>
                    <li>
                      <div class="mad-tt-title">Our Shop Timings</div>
                      Monday – Sunday: 6.30 AM – 9 PM <br/>
                    </li>
                    <li>
                      <div class="mad-tt-title">Delivery Time</div>
                      Monday – Sunday: 7 AM – 6 PM <br/>
                      Friday – Saturday: 2pm – 4pm
                    </li>
                  </ul>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    );
    
  }
}


export default Contact;