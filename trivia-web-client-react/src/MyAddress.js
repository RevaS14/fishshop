import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL } from './config';
import Swal from 'sweetalert2';


class MyAddress extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: null,
            email: null,
            mobile: null,
            address: null,
            notes: '',
            errorMessage: '',
            error: false,
            isLoading: false,
            cust: null
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
        this.getUser();
    }

    async getUser() {
        if (! this.state.cust) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/get-user/'+this.props.user.user_id);
                const orderList = await response.json();
                this.setState({ cust: orderList.data, isLoading: false});
                this.setState({ name: orderList.data[0].UserName, 
                    email: orderList.data[0].Email, 
                    mobile: orderList.data[0].Mobile, 
                    address: orderList.data[0].Address
                });
                console.log('add',orderList.data[0].Address);
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }

    async onSubmit(e) {
        e.preventDefault();
        this.setState({
            isLoading: true,
            error: false,
            errorMessage: ''
        });
  
        try {
  
        const response = await fetch(API_BASE_URL + '/update-address', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify({
                "name": this.state.name,
                "phone": this.state.mobile,
                "address": this.state.address,
                "user_id" : this.props.user.user_id
            })
        });
        const player = await response.json();
        Swal.fire({
          type: 'success',
          title: 'Thank you!',
          text: 'Your Address has been updated..',
        }).then(() => {
          //this.props.clearCart();
        });;
        // console.log(player);
  
      } catch (err) {
        console.error('err', err);
      }
    
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
    
        this.setState({
          [name]: value
        });
      }


    render() {
        
        return ( 
            
            (!this.props.user) ? (
                <div>
                    
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">My Address </h1>					
                    </div>
                   
                </div>
                
                    <div class="mad-content no-pd with-bg bg-gray">
                        <div class="container">
                            <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
                                {!this.props.user ? <h2 class="text-center">Please <a href={BASE_URL+'login'}>Login</a> to Continue </h2> :<h2 class="text-center">Please <a href={BASE_URL}>Shop</a> to Continue </h2>} 
                                
                            </div>
                        </div>
                    </div>
                 </div>
          ) : ( 
            <> 
            
            <div>
                
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">My Address </h1>					
                    </div>
                </div>
                <div class="mad-content no-pd bg-white">
        <div class="container">
          <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
            <div class="mad-colorizer-bg-color">
            <div class="with-svg-item svg-right-side"><img src={ BASE_URL +'assets/images/top_half_right.png'} alt="" /></div>
            </div>
            <div className="row vr-size-2 hr-size-1">
                <div class="col-md-3">
                  <div className="mad-sidebar">
                <div class="mad-widget">
                  <h4 class="mad-widget-title">My Account</h4>
                  <div class="mad-vr-list">
                    <ul>
                      <li><a href={BASE_URL + 'myaccount'} class="mad-link color-2">My Orders</a></li>
                      <li><a href={BASE_URL + 'myaddress'} class="mad-link color-2">My Address</a></li>
                      <li><a href="#" onClick={this.props.logout} class="mad-link color-2">Logout</a></li>
                    </ul>
                  </div>
                  </div>
                </div>
                </div>
                <div id="main" className="col-md-9">
                <div class="content-element-15">
                <h4 class="mad-page-title">My Address</h4>
                {this.state.cust && <div>
                <form error={this.state.error} onSubmit={this.onSubmit} class="mad-contact-form type-2 size-2">
                    <div class="mad-col">
                        <label>Name <span class="req">*</span></label>
                        <input type="text" value={this.state.name} onChange={this.handleInputChange} name="name" placeholder="Name" required  />
                      </div>
                      <div class="mad-col">
                        <label>Email Address <span class="req">*</span></label>
                        <input type="email" disabled value={this.state.email} onChange={this.handleInputChange} name="email" placeholder="Email Address" required />
                      </div>
                      <div class="mad-col">
                        <label>Phone Number <span class="req">*</span></label>
                        <input type="tel" value={this.state.mobile} onChange={this.handleInputChange} name="mobile" placeholder="Phone Number" required />
                      </div>
                      <div class="mad-col">
                        <label>Address <span class="req">*</span></label>
                        <textarea placeholder="Address" onChange={this.handleInputChange} name="address" required>{this.state.cust[0].Address}</textarea>
                      </div>

                      <div class="mad-col">
                     
                      {this.state.isLoading && <p class="btn btn-big order-btn">Loading..</p>}
                      {!this.state.isLoading &&
                         <label><button type="submit" loading={this.state.isLoading} class="btn btn-big order-btn">Update</button></label>
                      }
                      </div>
                   
                     
                    </form>
                    </div>
                }
              </div>
                </div>
            </div>
            
          </div>
        </div>
      </div>
                </div>
                </>
)
  );    
  }
}

export default MyAddress