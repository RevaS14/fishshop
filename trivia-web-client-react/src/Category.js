import React, { Component } from 'react';
import { Header, Message, Table } from 'semantic-ui-react';
import Product from './components/Product.js';
import { API_BASE_URL, BASE_URL } from './config';


class Category extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: null,
            isLoading: null,
            category : window.location.pathname.replace('/category/','')
        };
    }

    componentDidMount() {
        this.getPlayers();
    }

    async getPlayers() {
        if (! this.state.products) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/category/'+this.state.category);
                const playersList = await response.json();
                this.setState({ products: playersList, isLoading: false});
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }

    render() {
        console.log(this.state.products,'pro');
       
        return (
            <div>
                <div class="mad-breadcrumb with-bg title-center show-title-1">

<div class="container">

                
                                <h1 class="mad-page-title breadcrumb-title">{this.state.category}</h1>					
        

    
</div></div>
                <div class="mad-section mad-section--stretched mad-colorizer--scheme- explore our_products">
          
<div class="bubbles bubble-1"></div>
<div class="bubbles bubble-5"></div>
<div class="bubbles bubble-2"></div>
<div class="bubbles bubble-6"></div>
<div class="bubbles bubble-3"></div>
<div class="bubbles bubble-7"></div>
<div class="bubbles bubble-4"></div>
<div class="bubbles bubble-8"></div>
          <div class="mad-colorizer-bg-color" >
            <div class="with-svg-item bottom"><img src={ BASE_URL +'assets/images/fish_left.png'} alt="" /></div>
          </div>
          <div className="container">

          <div class="row">
          {!this.state.products || Object.keys(this.state.products.data).length===0 ? <h3>No Products Found</h3> : null}
          {this.state.products && this.state.products.data.length > 0 && this.state.products.data.map((product, index) =>
                
                    <Product
                    key={product.id}
                    product={product} index={index} addtocart={this.props.addcart} selected ={this.props.selected} handleChange ={this.props.handleChange} getPrice ={this.props.getPrice} currentProduct={this.props.currentProduct} variations={this.state.products.variations[index]} />
                
            )}

            

           

          </div>
          </div>
          
        </div>
            </div>
        );
    }

}

export default Category