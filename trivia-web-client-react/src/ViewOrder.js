import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL } from './config';
import { Redirect } from "react-router-dom";
import DataTable from 'react-data-table-component';
import Moment from 'moment';


class ViewOrder extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: null,
            order: null,
            shipping: null,
            orderId : window.location.pathname.replace('/vieworder/','')
        };

       
    }

    componentDidMount() {
        this.getOrders();
        this.getShipping();
        this.getOrder();
    }

    async getOrders() {
        if (! this.state.orders) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/vieworder/'+this.state.orderId);
                const orderList = await response.json();
                this.setState({ orders: orderList.data, isLoading: false});
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }

    async getShipping() {
      if (! this.state.shipping) {
          try {
              this.setState({ isLoading: true });
              const response = await fetch(API_BASE_URL + '/getshipping/'+this.state.orderId);
              const shippingList = await response.json();
              this.setState({ shipping: shippingList.data, isLoading: false});
          } catch (err) {
              this.setState({ isLoading: false });
              console.error(err);
          }
      }
  }

  async getOrder() {
    if (! this.state.order) {
        try {
            this.setState({ isLoading: true });
            const response = await fetch(API_BASE_URL + '/getorder/'+this.state.orderId);
            const orderData = await response.json();
            this.setState({ order: orderData.data, isLoading: false});
        } catch (err) {
            this.setState({ isLoading: false });
            console.error(err);
        }
    }
}

    


    render() {

       var total = 0;
        var delivery = 50;
        var tax = 20;      
        
        return ( 
            
            (!this.props.user) ? (
                <div>
                    
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Order Details {this.props.user.user_id}</h1>					
                    </div>
                   
                </div>
                
                    <div class="mad-content no-pd with-bg bg-gray">
                        <div class="container">
                            <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
                                {!this.props.user ? <h2 class="text-center">Please <a href={BASE_URL+'login'}>Login</a> to Continue </h2> :<h2 class="text-center">Please <a href={BASE_URL}>Shop</a> to Continue </h2>} 
                                
                            </div>
                        </div>
                    </div>
                 </div>
          ) : ( 
            <> 
            
            <div>
                
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Order Details </h1>					
                    </div>
                </div>
                <div class="mad-content no-pd bg-white">
        <div class="container">
          <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
            <div class="mad-colorizer-bg-color">
            <div class="with-svg-item svg-right-side"><img src={ BASE_URL +'assets/images/top_half_right.png'} alt="" /></div>
            </div>
            <div className="row vr-size-2 hr-size-1">
            <div className="col-md-12">
              <a href={ BASE_URL +'myaccount'} className="btn btn-success float-right" >Go Back</a>
            </div>
            </div>
            <div className="row vr-size-2 hr-size-1">
            <div className="col-md-3">
                  <h4>Shipping Address</h4>
                  {this.state.shipping &&
                  <div>
                    <p>{this.state.shipping[0].Name},
                      <br/>
                      {this.state.shipping[0].Email},
                      <br/>
                      {this.state.shipping[0].Mobile},
                      <br/>
                      {this.state.shipping[0].Address}
                      
                    </p>
                  </div>
                  } 
                  
                  </div>
                <div class="col-lg-9">
                <div class="content-element-15">
                <h4 class="mad-page-title">Order Summary</h4>
                <div class="order_info">
                {this.state.order &&
                <span class="bold"> Order #: {this.state.order[0].OrderNo}</span>
                }
                 {this.state.order &&
                <span class="bold float-right"> Date : {Moment(this.state.order[0].created_at).format('DD-MM-YYYY')}</span>
                }
                </div>
                <div class="mad-table-wrap shop-cart-form shop-acc-form">
                {this.state.orders &&
                <div class="mad-table-wrap">
                <table class="mad-table mad-table--vertical">
                 
                      <thead>
                          <tr>
                              <th></th>
                              <th>Name</th>
                              <th>Qty</th>
                              <th>Price</th> 
                          </tr>
                      </thead>
                      <tbody>
                      {this.state.orders.length > 0 && this.state.orders.map((product, index) =>
                    <tr>
                  <td>
                  <a href={BASE_URL+'product/'+product.ProductLink} className="mad-product-image">
                    <img width="100" src={BASE_URL + product.ImagePath} alt="" />
                  </a>
                  </td>
                 
                  <td>
                    <a href={BASE_URL+'product/'+product.ProductLink} className="mad-product-title mad-link">{product.ProductName}</a>
                    </td>
                    <td>{product.Size}</td>
                    <td>
                    <span className="mad-product-price"> Rs.{product.Price} <span className="d-none">{total += parseFloat(product.Price || 0)}</span></span>
                    </td>     
                </tr>
                 )}
                      
                    <tr>
                      <th colSpan="2"></th>
                      <th>Subtotal</th>
                      <td>Rs.{total}</td>
                    </tr>
                    <tr>
                    <th colSpan="2"></th>
                      <th>Delivery</th>
                      <td>Rs.50</td>
                    </tr>
                    <tr>
                    <th colSpan="2"></th>
                      <th>Tax</th>
                      <td>Rs.20</td>
                    </tr>
                  </tbody>
                  <tfoot>
                    <tr>
                    <th colSpan="2"></th>
                      <th>Total </th>
                      <td>Rs.{total+parseFloat(delivery)+parseFloat(tax)}</td>
                    </tr>
                  </tfoot>
                </table>
              </div>
              }
                  
                </div>
              </div>
                </div>
               
            </div>
            
          </div>
        </div>
      </div>
                </div>
                </>
)
  );    
  }
}

export default ViewOrder