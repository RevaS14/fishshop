import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL } from './config';
import { Redirect } from "react-router-dom";
import DataTable from 'react-data-table-component';
import Moment from 'moment';


class MyAccount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orders: null
        };

       
    }

    componentDidMount() {
        this.getOrders();
    }

    async getOrders() {
        if (! this.state.orders) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/userorders/'+this.props.user.user_id);
                const orderList = await response.json();
                this.setState({ orders: orderList.data, isLoading: false});
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }

    


    render() {

        const columns = [
            {
              name: 'No',
              selector: 'OrderId',
              sortable: true,
            },
            {
              name: 'Order #',
              selector: 'OrderNo',
              sortable: true,
            },
            {
              name: 'Total',
              cell: row => <div data-tag="allowRowEvents">Rs. {row.Total}</div>,
              sortable: true,
              right: true,
            },
            {
              name: 'Payment Method',
              cell: row => <div data-tag="allowRowEvents">{row.PaymentMethod=='C' ? 'Cash On Delivery': ''}</div>,
              sortable: true,
              right: true,
            },
            {
              name: 'Status',
              cell: row => <div data-tag="allowRowEvents">{row.Status=='P' ? 'Pending': row.Status=='A' ? 'Accepted' : row.Status=='C' ? 'Completed' :''}</div>,
              sortable: true,
              right: true,
            },
            {
              name: 'Ordered On',
              cell: row => <div data-tag="allowRowEvents">{Moment(row.created_at).format('DD-MM-YYYY')}</div>,
              sortable: true,
              right: true,
            },
            {
              name: 'Action',
              sortable: true,
              cell: row => <div data-tag="allowRowEvents"><a href={BASE_URL+'vieworder/'+row.OrderId} class="mad-link color-2">View</a></div>,
            }
          ];

          const data =  [{ "id": 1, "title": 'Conan the Barbarian', "year": '1982' }];
          console.log('orders',this.state.orders);
          //const data = JSON.parse(this.state.orders);
          //const data = this.state.orders;
        
        return ( 
            
            (!this.props.user) ? (
                <div>
                    
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">My Account </h1>					
                    </div>
                   
                </div>
                
                    <div class="mad-content no-pd with-bg bg-gray">
                        <div class="container">
                            <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
                                {!this.props.user ? <h2 class="text-center">Please <a href={BASE_URL+'login'}>Login</a> to Continue </h2> :<h2 class="text-center">Please <a href={BASE_URL}>Shop</a> to Continue </h2>} 
                                
                            </div>
                        </div>
                    </div>
                 </div>
          ) : ( 
            <> 
            
            <div>
                
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">My Account </h1>					
                    </div>
                </div>
                <div class="mad-content no-pd bg-white">
        <div class="container">
          <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
            <div class="mad-colorizer-bg-color">
            <div class="with-svg-item svg-right-side"><img src={ BASE_URL +'assets/images/top_half_right.png'} alt="" /></div>
            </div>
            <div className="row vr-size-2 hr-size-1">
                <div class="col-md-3">
                  <div className="mad-sidebar">
                <div class="mad-widget">
                  <h4 class="mad-widget-title">My Account</h4>
                  <div class="mad-vr-list">
                    <ul>
                      <li><a href={BASE_URL + 'myaccount'} class="mad-link color-2">My Orders</a></li>
                      <li><a href={BASE_URL + 'myaddress'} class="mad-link color-2">My Address</a></li>
                      <li><a href="#" onClick={this.props.logout} class="mad-link color-2">Logout</a></li>
                    </ul>
                  </div>
                  </div>
                </div>
                </div>
                <div id="main" className="col-md-9">
                <div class="content-element-15">
                <h4 class="mad-page-title">Order History</h4>
                <div class="mad-table-wrap shop-cart-form shop-acc-form">
                {this.state.orders &&
                <DataTable
                columns={columns}
                data={this.state.orders}
                pagination
              />
              }
                  
                </div>
              </div>
                </div>
            </div>
            
          </div>
        </div>
      </div>
                </div>
                </>
)
  );    
  }
}

export default MyAccount