import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL,DEV_BASE_URL } from './config';
import { Modal, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

class Navbar extends Component {

  

  openModal = () => this.setState({ isOpen: true });
  closeModal = () => this.setState({ isOpen: false });

  constructor(props) {
    super(props);
    this.state = {
        players: null,
        isLoading: null,
        submenus:null,
        locations:null,
        isOpen: false,
        location: null       
    };

}

  async componentDidMount() {
    //this.checkAuthentication();
    this.getPlayers();
    this.getsubmenu();
    this.getLocation();
    let loc = localStorage.getItem("location");
   
    if(!loc) {
      this.openModal();
    }
    this.setState({ location: loc }, () => {
    }); 
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
    localStorage.setItem("location", event.target.value);
    this.setState({ location:event.target.value });
    this.closeModal();
  };

  async getPlayers() {
    if (! this.state.players) {
        try {
            this.setState({ isLoading: true });
            const response = await fetch(API_BASE_URL + '/categories');
            const playersList = await response.json();
            this.setState({ players: playersList, isLoading: false});
        } catch (err) {
            this.setState({ isLoading: false });
            console.error(err);
        }
    }
}

async getLocation() {
  if (! this.state.locations) {
      try {
          this.setState({ isLoading: true });
          const response = await fetch(API_BASE_URL + '/getLocations');
          const locationsList = await response.json();
          this.setState({ locations: locationsList, isLoading: false});
      } catch (err) {
          this.setState({ isLoading: false });
          console.error(err);
      }
  }
}

  async componentDidUpdate() {
    //this.checkAuthentication();
  }

  async login() {
    //this.props.auth.login('/');
  }

  async logout() {
    //this.props.auth.logout('/');
  }

  async getsubmenu(){
    try {
      this.setState({ isLoading: true });
      const response = await fetch(API_BASE_URL + '/getSubMenu/1');
      const menuList = await response.json();
      this.setState({ submenus: menuList.data, isLoading: false});
    } catch (err) {
        this.setState({ isLoading: false });
        console.error(err);
    }
    return this.state.submenus;
  }

  render() {
    const data = this.props.cart;
    var total = 0;

    return (
      
      <div>
        {/* {
          
        <Menu fixed="top" inverted>
          <Container>
            <Menu.Item as="a" header href="/">
              Home
            </Menu.Item>
            <Menu.Item id="trivia-button" as="a" href="/trivia">Trivia Game</Menu.Item>
            <Menu.Item id="logout-button" as="a">Logout</Menu.Item>
            <Menu.Item as="a">Login</Menu.Item>
          </Container>
        </Menu> } */}
  
    <div id="mad-header" class="mad-header header-2 mad-header--transparent header-white">
      
      <div class="mad-header-section--sticky-xl">
      <div class="top-header">
          <div class="container">
            <div class="row">
            <div class="col-md-12 text-center">
              <span class="delivery-info">Delivery Only Available in Coimbatore!</span>
            </div>
            </div>
          </div>
        </div>
        <div class="container">
        
          <div class="mad-header-items">
            <div class="mad-header-item">
              <div id="stage">
                <a href="/" class="mad-logo" id="spinner"> 
                  <img width="150" src={BASE_URL + 'assets/images/logo1.png'} alt="" /> 
                </a>
              </div>
            </div>
            <div class="mad-header-item">
              <div class="slogan"> The Real Expert in Fresh Fish </div>
              <div class="shop_name">Thatha Fish Shop</div>
            </div>
            <nav class="mad-navigation-container">
              <ul class="mad-navigation mad-navigation--vertical-sm">
                <li class="menu-item current-menu-item"><a href="/">Home</a>
                 
                </li>
                {this.state.players && this.state.players.data.map((category, index) =>
                            <li id={category.CategoryId} key={category.CategoryId} className="menu-item menu-item-has-children">
                                    <a href="">{category.CategoryName}</a>
                                   
                                    {this.state.players.sub[index].length > 0 && <ul className="sub-menu"> {this.state.players.sub[index].map(
                                       menu =>
                                       <li id={menu.CategoryId} key={menu.CategoryId} className="menu-item">
                                               <a href={BASE_URL + 'category/' + menu.CategoryLink}> {menu.CategoryName}</a>
                                      </li>
                                    )}
                                    </ul>
                                  }
                                </li>
                        )}
               
                <li class="menu-item menu-item-has-children"><a href="/about-us">About Us</a>
                 
                </li>
                <li class="menu-item menu-item-has-children"><a href="/contact-us">Contact Us</a>
                 
                </li>
               
              </ul>
              <div class="mad-actions">
                <div class="mad-item mad-dropdown">
                  <a href="#" type="button" id="trivia-button" class="mad-item-link mad-dropdown-title"><i class="material-icons">person_outline</i></a>
                  <div class="user-dropdown shopping-cart mad-dropdown-element">
                    <div class="mad-products mad-product-small">
                      
                        {!this.props.user ? (
                          <ul class="sub-menu">
                          <li class="menu-item"><a href="/login">Login</a></li>
                          <li class="menu-item"><a href="/signup">Signup</a></li>
                          </ul>
                        ) : (
                          <ul class="sub-menu">
                          <li class="menu-item"><a href="/myaccount">My Account</a></li>
                          <li class="menu-item"><a href="#" onClick={this.props.logout}>Logout</a></li>
                          </ul>
                        )}
                       
                    </div>
                    </div>
                </div>
                <div class="mad-item">
                  <a href="#" type="button" id="trivia-button" onClick={this.openModal} class="mad-item-link mad-dropdown-title"><i class="material-icons">location_on</i></a>
                  
        <Modal className="location-modal" backdrop="static" show={this.state.isOpen} onHide={this.closeModal}>
          <Modal.Header>
            <Modal.Title>Select your location in Coimbatore</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <select className="form-control" onChange={this.handleChange} value={this.state.location}>
              <option value="">Select</option>
              {this.state.locations && this.state.locations.data.map((loc, index) =>
              <option value={loc.id}>{loc.name}</option>
              )}
              <option value="0">Other</option>
            </select>
          </Modal.Body>
          <Modal.Footer>
          
          </Modal.Footer>
        </Modal>
                </div>
                <div class="mad-item mad-dropdown">
                  <a href="#" type="button" class="mad-item-link mad-dropdown-title"><span class="mad-count">{this.props.cartcount}</span><i
                      class="material-icons">shopping_cart</i></a>
                  <div class="shopping-cart mad-dropdown-element">
                    <div class="mad-products mad-product-small">
                      
                    {data && Object.keys(data).map((d, key) => {
                        return (
                        <div className="mad-col">
                       
                        <div className="mad-product">
                          <button className="close-item" onClick={() => this.props.removeFromCart(d)} ><i className="licon-cross-circle"></i></button>
                          <a href="#" className="mad-product-image">
                            <img width="100" src={DEV_BASE_URL + data[d].product.ImagePath} alt="" />
                          </a>
                         
                          <div className="mad-product-description">
                            <a href="#" className="mad-product-title mad-link">{data[d].name}</a>
                            <span className="mad-product-price">1 × Rs.{data[d].amount} <span className="d-none">{total += parseFloat(data[d].amount || 0)}</span></span>
                          </div>
                         
                        </div>
                       
                      </div>
                          );
                        })}
                      {total == 0 &&
                          <div>Your cart is empty!</div>
                      }

                    </div>
                    
                    {total!=0 && 
                    <div class="sc-footer">
                      <div class="subtotal">Subtotal: <span>Rs.{total}</span></div>
                      <a href={BASE_URL + 'checkout'} class="btn w-100"><span>Checkout</span></a>
                    </div>}
                   
                  </div>
                </div>
                {/* <div class="mad-item">
                  <a href="#" class="mad-item-link" data-arctic-modal="#search-modal"><i class="material-icons">search</i></a>
                </div> */}
              </div>
            </nav>
           
          </div>
        </div>
      </div>
    </div>
    
     
      </div>
    );
  }
}

export default Navbar