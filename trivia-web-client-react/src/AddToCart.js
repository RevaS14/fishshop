import React from "react";

export default function AddToCart() {
    
    AddToCart = cartItem => {
        let cart = this.state.cart;
        if (cart[cartItem.id]) {
          cart[cartItem.id].amount += cartItem.amount;
        } else {
          cart[cartItem.id] = cartItem;
        }
        if (cart[cartItem.id].amount > cart[cartItem.id].product.stock) {
          cart[cartItem.id].amount = cart[cartItem.id].product.stock;
        }
        localStorage.setItem("cart", JSON.stringify(cart));
        this.setState({ cart });
        console.log(this.state.cart);
      };
  return <>Cart</>
}