import React from 'react';
import { API_BASE_URL, BASE_URL ,DEV_BASE_URL} from '../config'

import { Col, Panel } from 'react-bootstrap';

function getPrice(i) {
  return this.state.products.variations[i][this.state.selected].Price;
}

function Product(props) {
  let {
    product, index, variations, addtocart, selected, currentProduct, handleChange, getPrice
  } = props;


  return (
    <div className="col-md-3">
              <div class="card" data-prod="pr_jpekbard3vz">
                <a href={BASE_URL + 'product/'+product.ProductLink} data-text="" data-index="">
                  </a><div class="item-img"><a href="" data-text="" data-index="">
                    <picture>

                          <img class="product-image" alt=""  title="" src={DEV_BASE_URL + product.ImagePath} /></picture>
                          </a><div class="item-details"><a href={BASE_URL + 'product/'+product.ProductLink} data-text="" data-index="">
                            <div class="item-title">
                              <span class="product-name">{product.ProductName}</span></div>
                              <div class="item-desc">{product.ProductDescription}</div>
                            </a>
                              <p class="item-weight">
                              <span class="net-weight"> Net wt: 
                              {variations.length > 0 && 
                              <select class="form-control" id={product.ProductId} value={selected && currentProduct==product.ProductId ? selected : product.Price} onChange={handleChange}>
                                {variations.map((variation, vindex) =>
                                 <option value={variation.Price}>{variation.Size}</option>
                                  )}
                                </select> 
                                }
                                </span>
                              </p>
                      <div class="item-action">
                      <div class="rate"><span class="rupee remove-before"><span>MRP: </span> ₹{selected && currentProduct==product.ProductId ? selected : product.Price }</span><span class="rupee disc remove-before hide">₹235</span></div>
                      <div class="action">
                        <div class="action-slider">
                        {product.Stock ? (
                          <button class="add-to-cart addCartBtn-home" onClick={() =>
                              addtocart({
                                id: product.ProductId,
                                name: product.ProductName,
                                product : product,
                                amount: currentProduct==product.ProductId ? selected : product.Price 
                              })
                            }>Add To Cart</button>
                            ) : (
                              <button class="add-to-cart addCartBtn-home">Out Of Stock</button>
                            )}
                        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="product-message">
                        <div class="icon-message">
                        
                          <span class="message oos">Delivery 7AM - 9PM </span>
                        </div>
                      </div>
             
              </div>
            </div>
  );
}

export default Product;