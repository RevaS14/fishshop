import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL,DEV_BASE_URL } from './config';
import { Redirect } from "react-router-dom";
import Swal from 'sweetalert2';


class Checkout extends Component {

    constructor(props) {
        super(props);
        this.state = {
          name: (this.props.user ? this.props.user.user_name : '' ),
          email: (this.props.user ? this.props.user.email : '' ),
          mobile: (this.props.user ? this.props.user.mobile : '' ),
          address: (this.props.user ? this.props.user.address : '' ),
          pincode: (this.props.user ? this.props.user.pincode : '' ),
          notes: '',
          errorMessage: '',
          error: false,
          isLoading: false
      }
      this.handleInputChange = this.handleInputChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
    }

    componentDidMount() {
     
    }


    handleInputChange(event) {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
  
      this.setState({
        [name]: value
      });
    }

    async onSubmit(e) {
      e.preventDefault();
      this.setState({
          isLoading: true,
          error: false,
          errorMessage: ''
      });

      try {

      const response = await fetch(API_BASE_URL + '/orders', {
          method: 'POST',
          headers: {
              'Content-Type':'application/json',
              Accept: 'application/json'
          },
          body: JSON.stringify({
              "name": this.state.name,
              "email": this.state.email,
              "phone": this.state.mobile,
              "address": this.state.address,
              "pincode": this.state.pincode,
              "notes": this.state.notes,
              "cart" : this.props.cart,
              "user_id" : this.props.user.user_id,
              "location" : localStorage.getItem("location")
          })
      });
      const player = await response.json();
      this.setState({
        name: '',
        isLoading: false,
        error: false,
        errorMessage: ''
      });
      Swal.fire({
        type: 'success',
        title: 'Thank you!',
        text: 'Your Order has been placed successfully..',
      }).then(() => {
        this.props.clearCart();
      });;
      // console.log(player);

      // if (player.errors) {
      //     this.setState({
      //         isLoading: false,
      //         error: true,
      //         errorMessage: player.errors
      //     });
      // } else {
      //     this.setState({
      //         name: '',
      //         isLoading: false,
      //         error: false,
      //         errorMessage: ''
      //     });
      // }

    } catch (err) {
      console.error('err', err);
    }
  
  }

    render() {
        const data = this.props.cart;
        const userdata = this.props.user;
        var total = 0;
        var delivery = 50;
        var tax = 20;      
        console.log(this.props.user);
        return ( 
            (!this.props.user || Object.keys(data).length === 0) ? (
                <div>
                    
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Checkout </h1>					
                    </div>
                   
                </div>
                
                    <div class="mad-content no-pd with-bg bg-gray">
                        <div class="container">
                            <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
                                {!this.props.user ? <h2 class="text-center">Please <a href={BASE_URL+'login'}>Login</a> to Continue </h2> :<h2 class="text-center">Please <a href={BASE_URL}>Shop</a> to Continue </h2>} 
                                
                            </div>
                        </div>
                    </div>
                 </div>
          ) : (
            <> <div>
                
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Checkout </h1>					
                    </div>
                </div>
                <div class="mad-content no-pd bg-white">
        <div class="container">
          <div class="mad-section mad-section--stretched mad-colorizer--scheme-">
            <div class="mad-colorizer-bg-color">
              <div class="with-svg-item svg-right-side"><img src="images/top_half_right.png" alt="" /></div>
            </div>
            
            <div class="content-element-15">
              <div class="row vr-size-5">
                <div class="col-lg-6">
                <div class="content-element-15">
                    <h4>Billing Details</h4>
                    
                    <form error={this.state.error} onSubmit={this.onSubmit} class="mad-contact-form type-2 size-2">
                    <div class="mad-col">
                        <label>Name <span class="req">*</span></label>
                        <input type="text" value={this.state.name} onChange={this.handleInputChange} name="name" placeholder="Name" required  />
                      </div>
                      <div class="mad-col">
                        <label>Email Address <span class="req">*</span></label>
                        <input type="email" value={this.state.email} onChange={this.handleInputChange} name="email" placeholder="Email Address" required />
                      </div>
                      <div class="mad-col">
                        <label>Phone Number <span class="req">*</span></label>
                        <input type="tel" value={this.state.mobile} onChange={this.handleInputChange} name="phone" placeholder="Phone Number" required />
                      </div>
                      <div class="mad-col">
                        <label>Address <span class="req">*</span></label>
                        <textarea placeholder="Address" onChange={this.handleInputChange} name="address" required>{this.state.address}</textarea>
                      </div>
                      <div class="mad-col">
                        <label>Pincode <span class="req">*</span></label>
                        <input type="number" value={this.state.pincode} onChange={this.handleInputChange} placeholder="Pincode" name="pincode" required />
                      </div>
                     
                      <div class="mad-col">
                      <h4> Payment Method</h4>
                      <div class="form-control payment-option">
                      <div  class="mad-toggled-fields">
                        <input type="radio" name="radio-5" id="radio-5" checked="checked" />
                        <label for="radio-5" class="mad-label-radio">
                          <b>Cash on delivery</b> <br/>
                         </label>
                        <input type="radio" name="radio-5" id="radio-6" />
                       
                      </div>
                    </div>
                      </div>
                      <div class="mad-col">
                      <h4>Additional Information</h4>
                        <label>Order notes:</label>
                        <textarea rows="5" onChange={this.handleInputChange} name="notes">{this.state.notes}</textarea>
                      </div>
                      <div class="mad-col">
                     
                      {this.state.isLoading && <p class="btn btn-big order-btn">Loading..</p>}
                      {!this.state.isLoading &&
                         <label><button type="submit" loading={this.state.isLoading} class="btn btn-big order-btn">Place Order</button></label>
                      }
                      </div>
                   
                     
                    </form>
                  </div>
                </div>
                <div class="col-lg-6">
                <div class="content-element-15">
                    <h4>Order Summary</h4>
                    <div class="content-element-4">
                      <div class="mad-table-wrap">
                        <table class="mad-table mad-table--vertical">
                         
                              <thead>
                                  <tr>
                                      <th></th>
                                      <th>Name</th>
                                      <th>Qty</th>
                                      <th>Price</th> 
                                  </tr>
                              </thead>
                              <tbody>
                              {data && Object.keys(data).map((d, key) => {
                        return (
                            <tr>
                       <td>
                          <a href={BASE_URL+'product/'+data[d].product.ProductLink} className="mad-product-image">
                            <img width="100" src={DEV_BASE_URL + data[d].product.ImagePath} alt="" />
                          </a>
                          </td>
                         
                          <td>
                            <a href={BASE_URL+'product/'+data[d].product.ProductLink} className="mad-product-title mad-link">{data[d].name}</a>
                            </td>
                            <td>{data[d].product.Size}</td>
                            <td>
                            <span className="mad-product-price"> Rs.{data[d].amount} <span className="d-none">{total += parseFloat(data[d].amount || 0)}</span></span>
                            </td>     
                        </tr>
                          );
                        })}
                              
                            <tr>
                              <th colSpan="2"></th>
                              <th>Subtotal</th>
                              <td>Rs.{total}</td>
                            </tr>
                            <tr>
                            <th colSpan="2"></th>
                              <th>Delivery</th>
                              <td>Rs.50</td>
                            </tr>
                            <tr>
                            <th colSpan="2"></th>
                              <th>Tax</th>
                              <td>Rs.20</td>
                            </tr>
                          </tbody>
                          <tfoot>
                            <tr>
                            <th colSpan="2"></th>
                              <th>Total </th>
                              <td>Rs.{total+parseFloat(delivery)+parseFloat(tax)}</td>
                            </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                   
                  </div>
                  
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
                </div>
                </>
)
  );    
  }
}

export default Checkout