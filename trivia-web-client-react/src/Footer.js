import React, { Component } from 'react';
import { API_BASE_URL } from './config'

class Footer extends Component {

  render() {
    return (
      <div>
      
    <div id="mad-header" class="mad-header header-2 mad-header--transparent header-white">
      
      <div class="mad-header-section--sticky-xl">
        <div class="container">
          <div class="mad-header-items">
            <div class="mad-header-item">
              <div id="stage">
                <a href="index.html" class="mad-logo" id="spinner"> 
                  <img width="150" src="assets/images/logo1.png" alt="" /> 
                </a>
              </div>
            </div>
            <div class="mad-header-item">
              <div class="slogan"> The Real Expert in Fresh Fish </div>
              <div class="shop_name">Thatha Fish Shop</div>
            </div>
            <nav class="mad-navigation-container">
              <ul class="mad-navigation mad-navigation--vertical-sm">
                <li class="menu-item current-menu-item"><a href="#">Home</a>
                 
                </li>
                {this.state.players && this.state.players.map(
                            category =>
                            <li id={category.CategoryId} key={category.CategoryId} className="menu-item menu-item-has-children">
                                    <a href="">{category.CategoryName}</a>
                                   
                                    {this.state.submenus && <ul className="sub-menu"> {this.state.submenus.map(
                                       menu =>
                                       <li id={menu.CategoryId} key={menu.CategoryId} className="menu-item">
                                               <a href="">{menu.CategoryName}</a>
                                      </li>
                                    )}
                                    </ul>
                                  }
                                </li>
                        )}
               
                <li class="menu-item menu-item-has-children"><a href="#">About Us</a>
                 
                </li>
                <li class="menu-item menu-item-has-children"><a href="#">Contact Us</a>
                 
                </li>

              </ul>
              <div class="mad-actions">
                <div class="mad-item">
                  <a href="#" class="mad-item-link"><i class="material-icons">person_outline</i></a>
                </div>
                <div class="mad-item mad-dropdown">
                  <a href="#" type="button" class="mad-item-link mad-dropdown-title"><span class="mad-count">3</span><i
                      class="material-icons">shopping_cart</i></a>
                  <div class="shopping-cart mad-dropdown-element">
                    <div class="mad-products mad-product-small">
                      <div class="mad-col">
                       
                        <div class="mad-product">
                          <button class="mad-close-item"><i class="licon-cross-circle"></i></button>
                          <a href="#" class="mad-product-image">
                            <img src="images/72x72_img1.jpg" alt="" />
                          </a>
                         
                          <div class="mad-product-description">
                            <a href="#" class="mad-product-title mad-link">Crab Oscar</a>
                            <span class="mad-product-price">1×$30</span>
                          </div>
                         
                        </div>
                       
                      </div>
                      <div class="mad-col">
                       
                        <div class="mad-product">
                          <button class="mad-close-item"><i class="licon-cross-circle"></i></button>
                          <a href="#" class="mad-product-image">
                            <img src="images/72x72_img2.jpg" alt="" />
                          </a>
                          
                          <div class="mad-product-description">
                            <a href="#" class="mad-product-title mad-link">Burrata Salad</a>
                            <span class="mad-product-price">1×$35</span>
                          </div>
                          
                        </div>
                        
                      </div>
                    </div>
                    <div class="sc-footer">
                      <div class="subtotal">Subtotal: <span>$21.98</span></div>
                      <a href="#" class="btn w-100"><span>Checkout</span> <i><img src="lemar_svg_icons/short_arrow_right.svg"
                            alt="" class="svg" /></i></a>
                    </div>
                  </div>
                </div>
                <div class="mad-item">
                  <a href="#" class="mad-item-link" data-arctic-modal="#search-modal"><i class="material-icons">search</i></a>
                </div>
              </div>
            </nav>
           
          </div>
        </div>
      </div>
    </div>
    
     
      </div>
    );
  }
}

export default Navbar