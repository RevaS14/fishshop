import React, { Component } from 'react';
import { API_BASE_URL } from './config';
import { Redirect } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      logStatus: ""
    };
  } 
  handleChange = e => this.setState({ [e.target.name]: e.target.value, error: "" });

  login = (e) => {
    e.preventDefault();

    const { username, password } = this.state;
    if (!username || !password) {
      return this.setState({ error: "Fill all fields!" });
    }
    this.props.login(username, password)
      .then((loggedIn) => {
        if (!loggedIn) {
          this.setState({ error: "Invalid Credentails" });
        } else if(loggedIn=='Not found') {
          this.setState({ error: "Sorry! Mobile number not found.." });
        } 
      })
  };


render() {
  return ( (this.props.user) ? (
    <Redirect to="/" />
  ) : (
    <> <div>
       <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Login</h1>					
                    </div>
                </div>
<div class="mad-content no-pd with-bg">
        <div class="container">
          <div class="mad-section pad-top-0 mad-section--stretched mad-colorizer--scheme-">
          <div class="bubbles bubble-1"></div>
<div class="bubbles bubble-5"></div>
<div class="bubbles bubble-2"></div>
<div class="bubbles bubble-6"></div>
<div class="bubbles bubble-3"></div>
<div class="bubbles bubble-7"></div>
<div class="bubbles bubble-4"></div>
<div class="bubbles bubble-8"></div>
            <div class="mad-colorizer-bg-color">
              <div class="with-svg-item bottom2"><img src="images/bottom_half_left.png" alt="" /></div>
            </div>
            <div class="row">
              <div class="col-xl-8">
              {this.state.error && (
                          <div className="text-danger">{this.state.error}</div>
              )}
                  <div class="col-md-7 offset-md-5">
                  <h3 class="mad-page-title text-white">Login</h3>
                    <form class="mad-contact-form size-2" onSubmit={this.login}>
                      <div class="mad-col">
                        <input type="text" id="cf_name" onChange={this.handleChange} name="username" required placeholder="Mobile" />
                      </div>
                      <div class="mad-col">
                        <input type="password" id="cf_email" name="password" onChange={this.handleChange} required placeholder="Password" />
                      </div>
                      
                      <div class="mad-col">
                        <button type="submit" class="btn"><span>Login</span></button>
                      </div>
                    </form>
                  </div>
                </div>
             
              
            </div>
          </div>
        </div>
      </div>
      </div>
      </>
)
  );    
  }
}


export default Login;