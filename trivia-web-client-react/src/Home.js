import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL, DEV_BASE_URL } from './config';
import { Carousel } from 'react-bootstrap';


class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
        sliders: null,
        products: null,
        selected:null,
        currentProduct:null
    };
    this.handleChange = this.handleChange.bind(this); 
    //this.props.addToCart.bind(this);
    //console.log('cart' + this.props.addcart);
    console.log(props);
    //console.log(JSON.stringify(this.props));
    
    //this.state.selected = this.state.selected.bind(this);   
    

}

// addToCart() {
//   this.props.addToCart(this.state)
// }

async componentDidMount() {
  //console.log(props.count);

  
  this.getSliders();
  this.getCategories();
  this.getProducts();

}



handleChange(event) {
  this.setState({selected: event.target.value});
  this.setState({currentProduct: event.target.id});
  //this.getPrice();
}

async getSliders() {
  if (! this.state.sliders) {
      try {
          this.setState({ isLoading: true });
          const response = await fetch(API_BASE_URL + '/sliders');
          const slidersList = await response.json();
          this.setState({ sliders: slidersList.data, isLoading: false});
      } catch (err) {
          this.setState({ isLoading: false });
          console.error(err);
      }
  }
}

async getProducts() {
  if (! this.state.products) {
      try {
          this.setState({ isLoading: true });
          const response = await fetch(API_BASE_URL + '/products');
          const productsList = await response.json();
          this.setState({ products: productsList, isLoading: false});
      } catch (err) {
          this.setState({ isLoading: false });
          console.error(err);
      }
  }
}

getPrice(i) {
    return this.state.products.variations[i][this.state.selected].Price;
}

async getCategories() {
  if (! this.state.categories) {
      try {
          this.setState({ isLoading: true });
          const response = await fetch(API_BASE_URL + '/getSubMenu/1');
          const categoryList = await response.json();
          this.setState({ categories: categoryList.data, isLoading: false});
      } catch (err) {
          this.setState({ isLoading: false });
          console.error(err);
      }
  }
}


    render() {
      const config = {
        delay:9000,
        startwidth:1170,
        startheight:500,
        hideThumbs:10,
        fullWidth:"on",
        forceFullWidth:"on"
    }; 

        return ( 

          
          
        <div>
      <div class="banner-grid">
      <Carousel>
      {this.state.sliders && this.state.sliders.map(
                                        slider =>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={DEV_BASE_URL + slider.Image}
            alt="Thatha Fish Shop"
          />
        
        </Carousel.Item>
        )}
        
      </Carousel>
      </div>
        

        <div className="mad-content no-pd">
        <div className="bg-gray">
        <div class="container">
            <div class="mad-section mad-section--stretched mad-colorizer--scheme-color-4 category_section"><div class="mad-colorizer-bg-color" ></div>
            
                <h3 class="text-center mad-title category_title">Shop By Categories</h3>
              
                <div class="row">
                  {this.state.categories && this.state.categories.map(
                                  category =>
                                  
                  <div class="col-md-3">
                    <a href={BASE_URL + 'category/' + category.CategoryLink}>
                      <div class="cat_box">
                      
                        <div class="cat_image">
                          <img src={DEV_BASE_URL + category.CategoryImage} alt="" />
                        </div>
                        <div class="cat_title">
                        {category.CategoryName}  
                        </div>
                      </div>
                    </a>
                  </div>
                  )}
                
                </div>
            </div>
        </div>
        </div>
        
        <div class="mad-section pad-top-0 mad-section--stretched mad-colorizer--scheme- explore our_products">
        <div class="fish">
  <div class="fish-body">
    <div class="eye">
      <div class="pupil"></div>
    </div>
  </div>
  <div class="fin"></div>
  <div class="fin fin-bottom"></div>
</div>

<div class="bubbles bubble-1"></div>
<div class="bubbles bubble-5"></div>
<div class="bubbles bubble-2"></div>
<div class="bubbles bubble-6"></div>
<div class="bubbles bubble-3"></div>
<div class="bubbles bubble-7"></div>
<div class="bubbles bubble-4"></div>
<div class="bubbles bubble-8"></div>








          <div class="mad-colorizer-bg-color" >
            <div class="with-svg-item bottom"><img src="assets/images/fish_left.png" alt="" /></div>
          </div>
          <div className="container">
          <h3 class="text-center mad-title category_title">Latest Products</h3>

          <div class="row">
          {this.state.products && this.state.products.data.map((product, index) =>
            <div className="col-md-3">
              <div class="card" data-prod="pr_jpekbard3vz">
                <a href="" data-text="" data-index="">
                  </a><div class="item-img"><a href={BASE_URL + 'product/'+product.ProductLink} data-text="" data-index="">
                    <picture>

                          <img class="product-image" alt=""  title="" src={DEV_BASE_URL + product.ImagePath} /></picture>
                          </a><div class="item-details"><a href={BASE_URL + 'product/'+product.ProductLink} data-text="" data-index="">
                            <div class="item-title">
                              <span class="product-name">{product.ProductName}</span></div>
                              <div class="item-desc">{product.ProductDescription}</div>
                            </a>
                              <p class="item-weight">
                              <span class="net-weight"> Net wt: 
                              {this.state.products.variations[index].length > 0 && 
                              <select class="form-control" id={product.ProductId} value={this.state.value} onChange={this.handleChange}>
                                {this.state.products.variations[index].map((variation, vindex) =>
                                 <option value={vindex} selected={product.ProductVariationId == variation.ProductVariationId}>{variation.Size}</option>
                                  )}
                                </select> 
                                }
                                </span>
                              </p>
                      <div class="item-action">
                        <div class="rate"><span class="rupee remove-before"><span>MRP: </span> ₹{this.state.selected && this.state.currentProduct==product.ProductId ? this.getPrice(index) : product.Price }</span><span class="rupee disc remove-before hide">₹235</span></div>
                      <div class="action">
                        <div class="action-slider"> 
                              {product.Stock ? (
                                  <button class="add-to-cart addCartBtn-home" data-cat="
  
                                  Fish &amp; Seafood
                                " data-amt="235"  data-index="1" data-prid="pr_6j4jhlmxsk1" data-qty="11" data-recommended="undefined" onClick={() =>
                                  this.props.addcart({
                                    id: product.ProductId,
                                    name: product.ProductName,
                                    product: product,
                                    amount: this.state.currentProduct==product.ProductId ? this.getPrice(index) : product.Price 
                                  })
                                }>Add To Cart</button>
                                  ) : (
                                    <button class="add-to-cart addCartBtn-home">Out Of Stock</button>
                                  )}
                        
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  <div class="product-message">
                        <div class="icon-message">
                        
                          <span class="message oos">Delivery 7AM - 9PM </span>
                        </div>
                      </div>
             
              </div>
            </div>

            )}

          </div>
          </div>
          
        </div>
        <div>
        <div data-bg-image-src="assets/images/1922x1008_bg1.jpg" class="mad-section with-overlay mad-section--stretched mad-colorizer--scheme- mad-colorizer--scheme-light testimonials"><div class="mad-colorizer-bg-image" ></div><div class="mad-colorizer-bg-color"></div>
       
        <div class="mad-offset testimonial-grid">
            <h2 class="mad-title content-element-7">Clients Testimonials</h2>
            <div class="mad-testimonials style-2">
              <div class="mad-grid">
            <Carousel>
            <Carousel.Item>
              <div class="mad-grid-item">
                  <div class="mad-testimonial">
                    <div data-estimate="5" class="mad-rating">
                      <div class="mad-rating-top-level">
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        </div>
                    
                    </div>
                    <div class="mad-testiomonial-info">
                      <blockquote>
                        <p>“Best Experience with your shop..and good quality fish</p>
                      </blockquote>
                    </div>
                    <div class="mad-author">
                      <div class="mad-author-info">
                        <cite>Sunder, Coimbatore</cite>
                        <span><img src="images/visor.png" alt=""/></span>
                      </div>
                    </div>
                  </div>
                </div>
            
            </Carousel.Item>
            <Carousel.Item>

            <div class="mad-grid-item">
                  <div class="mad-testimonial">
                    <div data-estimate="5" class="mad-rating">
                      <div class="mad-rating-top-level">
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        </div>
                    
                    </div>
                    <div class="mad-testiomonial-info">
                      <blockquote>
                        <p>“Best Experience with your shop..and good quality fish</p>
                      </blockquote>
                    </div>
                    <div class="mad-author">
                      <div class="mad-author-info">
                        <cite>Sunder, Coimbatore</cite>
                        <span><img src="images/visor.png" alt=""/></span>
                      </div>
                    </div>
                  </div>
                </div>
            
            </Carousel.Item>
            <Carousel.Item>

            <div class="mad-grid-item">
                  <div class="mad-testimonial">
                    <div data-estimate="5" class="mad-rating">
                      <div class="mad-rating-top-level">
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        <i class="material-icons active">star</i>
                        </div>
                    
                    </div>
                    <div class="mad-testiomonial-info">
                      <blockquote>
                        <p>“Best Experience with your shop..and good quality fish</p>
                      </blockquote>
                    </div>
                    <div class="mad-author">
                      <div class="mad-author-info">
                        <cite>Sunder, Coimbatore</cite>
                        <span><img src="images/visor.png" alt=""/></span>
                      </div>
                    </div>
                  </div>
                </div>
            
            </Carousel.Item>
            
          </Carousel>
          </div>
          </div>
          </div>
        </div> 
        
        
  
      </div>
        </div>

        </div>
        
        );
        
    }
}

export default Home