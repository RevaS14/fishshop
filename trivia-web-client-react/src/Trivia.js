import React, { Component } from 'react';
import { Header, Message, Table } from 'semantic-ui-react';


import { API_BASE_URL } from './config'

class Trivia extends Component {

    constructor(props) {
        super(props);
        this.state = {
            players: null,
            isLoading: null,
            url : null,
            category: null
        };
    }

    componentDidMount() {
        this.getPlayers();
        this.state.url = this.props.location.pathname;
        this.state.category = this.state.url.replace('/trivia/','');
        console.log(this.state.category,'category');
    }

    async getPlayers() {
        if (! this.state.players) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/categories');
                const playersList = await response.json();
                this.setState({ players: playersList.data, isLoading: false});
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }

    render() {
        return (
            <div>
                <Header as="h1">Players</Header>
                {this.state.isLoading && <Message info header="Loading players..." />}
                {this.state.players &&
                    <div>
                        <Table>
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category Name</th>
                                    <th>Category Name</th>
                                    <th>Category Link</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.players.map(
                                    category =>
                                        <tr id={category.CategoryId} key={category.CategoryId}>
                                            <td>{category.CategoryId}</td>
                                            <td>{category.CategoryName}</td>
                                            <td>{category.CategoryLink}</td>
                                            <td>{category.Status}</td>
                                            <td>
                                                Action buttons placeholder
                                            </td>
                                        </tr>
                            )}
                            </tbody>
                        </Table>
                    </div>
                }
            </div>
        );
    }

}

export default Trivia