import React, { Component } from 'react';
import { API_BASE_URL } from './config';
import { Redirect } from "react-router-dom";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      mobile: "",
      address: "",
      password: "",
      cpassword: "",
      logStatus: ""
    };
  } 
  handleChange = e => this.setState({ [e.target.name]: e.target.value, error: "" });

  login = (e) => {
    e.preventDefault();

    const { username, email, mobile, address, password, cpassword } = this.state;
    if (!username || !password || !email || !mobile || !address) {
      return this.setState({ error: "Fill all fields!" });
    }

    if(password != cpassword) {
        return this.setState({ error: "Confirm password not match!" });
    }

    this.props.signup(username, email, mobile, address, password)
      .then((loggedIn) => {
        if (!loggedIn) {
          this.setState({ error: "Something Went Wrong..!" });
        } else if(loggedIn=='Not found') {
          this.setState({ error: "Sorry! user not found.." });
        } else if(loggedIn=='Email') {
        this.setState({ error: "Email Already Exists..!" });
        } else if(loggedIn=='Mobile') {
        this.setState({ error: "Mobile Number Already Exists..!" });
        } 
      })
  };


render() {
  return ( (this.props.user) ? (
    <Redirect to="/" />
  ) : (
    <> <div>
       <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">Signup</h1>					
                    </div>
                </div>
<div class="mad-content no-pd with-bg">
        <div class="container">
          <div class="mad-section pad-top-0 mad-section--stretched mad-colorizer--scheme-">
          <div class="bubbles bubble-1"></div>
<div class="bubbles bubble-5"></div>
<div class="bubbles bubble-2"></div>
<div class="bubbles bubble-6"></div>
<div class="bubbles bubble-3"></div>
<div class="bubbles bubble-7"></div>
<div class="bubbles bubble-4"></div>
<div class="bubbles bubble-8"></div>
            <div class="mad-colorizer-bg-color">
              <div class="with-svg-item bottom2"><img src="images/bottom_half_left.png" alt="" /></div>
            </div>
            <div class="row">
              <div class="col-xl-8">
             
                  <div class="col-md-7 offset-md-5">
                  {this.state.error && (
                          <div className="alert alert-danger"> <div className="text-danger">{this.state.error}</div></div>
              )}
                  <h3 class="mad-page-title text-white">Signup</h3>
                    <form class="mad-contact-form size-2" onSubmit={this.login}>
                      <div class="mad-col">
                        <input type="text" id="username" onChange={this.handleChange} name="username" required placeholder="User Name" />
                      </div>
                      <div class="mad-col">
                        <input type="text" id="email" onChange={this.handleChange} name="email" required placeholder="Email" />
                      </div>
                      <div class="mad-col">
                        <input type="text" id="mobile" onChange={this.handleChange} name="mobile" required placeholder="Mobile" />
                      </div>
                      <div class="mad-col">
                          <textarea name="address" id="address" onChange={this.handleChange} placeholder="Address" required></textarea>
                      </div>
                      <div class="mad-col">
                        <input type="password" id="password" name="password" onChange={this.handleChange} required placeholder="Password" />
                      </div>
                      <div class="mad-col">
                        <input type="password" id="cpassword" name="cpassword" onChange={this.handleChange} required placeholder="Confirm Password" />
                      </div>
                      
                      <div class="mad-col">
                        <button type="submit" class="btn"><span>Signup</span></button>
                      </div>
                    </form>
                  </div>
                </div>
             
              
            </div>
          </div>
        </div>
      </div>
      </div>
      </>
)
  );    
  }
}


export default Signup;