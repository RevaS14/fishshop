import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { API_BASE_URL } from './config';
import Swal from 'sweetalert2';
// import axios from 'axios';

import Navbar from './Navbar';
import Home from './Home';
import Trivia from './Trivia';
import Login from './Login';
import Signup from './Signup';
import Category from './Category';
import Product from './Product';
import Checkout from './Checkout';
import MyAccount from './MyAccount';
import ViewOrder from './ViewOrder';
import MyAddress from './MyAddress';
import About from './About';
import Contact from './Contact';


class App extends Component {
  constructor(props) {
    super(props);
    const { match, location, history } = props;

    let user = localStorage.getItem("user");
    user = user ? JSON.parse(user) : null;
    
    this.state = {
      user: user,
      cart: {},
      selected:null,
      currentProduct:null,
      categories:null
    };
  //this.props.addToCart.bind(this);
  this.handleChange = this.handleChange.bind(this); 

}


async componentDidMount() {
  this.getPlayers();
  //console.log(props.count);
  let user = localStorage.getItem("user");
  let cart = localStorage.getItem("cart");

  user = user ? JSON.parse(user) : null;
  cart = cart? JSON.parse(cart) : {};

  this.setState({ user, cart });
  console.log('user' + user);
}

handleChange(event) {
  
  this.setState({selected: event.target.value});
  this.setState({currentProduct: event.target.id});
  //this.getPrice();
  
}

async getPlayers() {
  if (! this.state.categories) {
      try {
          this.setState({ isLoading: true });
          const response = await fetch(API_BASE_URL + '/categories');
          const playersList = await response.json();
          this.setState({ categories: playersList, isLoading: false});
      } catch (err) {
          this.setState({ isLoading: false });
          console.error(err);
      }
  }
}


  addToCart = cartItem => {
    let cart = this.state.cart;
    let loc = localStorage.getItem("location");
    if(loc==0) {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Sorry! Delivery not available to your area',
        // footer: '<a href>Why do I have this issue?</a>'
      });
      return false;
    }
    // if (cart[cartItem.id]) {
    //   //cart[cartItem.id].amount += cartItem.amount;
    // } else {
    //   cart[cartItem.id] = cartItem;
    // }
    cart[cartItem.id] = cartItem;
    //cart[cartItem.id] = cartItem;
    // if (cart[cartItem.id].amount > cart[cartItem.id].product.Stock) {
    //   cart[cartItem.id].amount = cart[cartItem.id].product.Stock;
    // }
    localStorage.setItem("cart", JSON.stringify(cart));
    this.setState({ cart });
    console.log(this.state.cart,'cart');
    Swal.fire({
      type: 'success',
      title: 'Thank you!',
      text: 'Product Added to your cart',
      // footer: '<a href>Why do I have this issue?</a>'
    });

  };

  //contact starts

  contact = async (name, email, mobile, message) => {
    const response = await fetch(API_BASE_URL + '/contact', {
        method: 'POST',
        headers: {
            'Content-Type':'application/pjson',
            Accept: 'application/json'
        },
        body: JSON.stringify({
            "name": name,
            "email": email,
            "mobile": mobile,
            "message": message
        })
    });
    const result = await response.json();
  }

  //contact end

      login = async (email, password) => {

        const response = await fetch(API_BASE_URL + '/login', {
          method: 'POST',
          headers: {
              'Content-Type':'application/json',
              Accept: 'application/json'
          },
          body: JSON.stringify({
              "username": email,
              "password": password
          })
      });
      const player = await response.json();
      console.log(player);
      if (player[0]=='Not found') {
        return 'Not found';
      } else if(player[0]=='Invalid') {
        return false;
      }
      else {
          const user = {
            user_id : player.UserId,
            user_name : player.UserName,
            email : player.Email,
            mobile : player.Mobile,
            address : player.Address,
          }
          this.setState({ user });
          localStorage.setItem("user", JSON.stringify(user));
        
          return true;
      }

  }

  //signup starts

    signup = async (username, email, mobile, address, password) => {

      const response = await fetch(API_BASE_URL + '/signup', {
        method: 'POST',
        headers: {
            'Content-Type':'application/json',
            Accept: 'application/json'
        },
        body: JSON.stringify({
            "username": username,
            "mobile": mobile,
            "email": email,
            "mobile": mobile,
            "address": address,
            "password": password        
          })  
    });

    const player = await response.json();
    // alert(player);
    //  return false;
    if (player[0]=='Not found') {
      return 'Not found';
    } else if(player[0]=='Email') {
      return 'Email';
    } else if(player[0]=='Mobile') {
      return 'Mobile';
    }
    else {
        const user = {
          user_id : player.UserId,
          user_name : player.UserName,
          email : player.Email,
          mobile : player.Mobile,
          address : player.Address,
        }
        this.setState({ user });
        localStorage.setItem("user", JSON.stringify(user));
      
        return true;
    }
  }
  //signup ends
  
  logout = e => {
    e.preventDefault();
    this.setState({ user: null });
    localStorage.removeItem("user");
    console.log(this.props);
    //this.props.history.push("/");
  };

  removeFromCart = cartItemId => {
    let cart = this.state.cart;
    delete cart[cartItemId];
    localStorage.setItem("cart", JSON.stringify(cart));
    this.setState({ cart });
  };
  
  clearCart = () => {
    let cart = {};
    localStorage.removeItem("cart");
    this.setState({ cart });
  };

  getCartCount() {
    return Object.keys(this.state.cart).length;
  }

  render() {

    return (
        <Router>
          <div>
            <Navbar cartcount={this.getCartCount()} cart={this.state.cart} removeFromCart={this.removeFromCart} user={this.state.user} logout={this.logout} />
            
                <Route path="/" exact render={(props) => (<Home addcart={this.addToCart} />)}  />
                <Route path="/login" exact render={(props) => (<Login login={this.login} user={this.state.user}/>)} />
                <Route path="/signup" exact render={(props) => (<Signup signup={this.signup} user={this.state.user}/>)} />
                <Route path="/myaccount" exact render={(props) => (<MyAccount user={this.state.user}/>)} />
                <Route path="/trivia" component={Trivia} />
                <Route path="/category/:link" exact render={(props) => (<Category addcart={this.addToCart} handleChange ={this.handleChange} getPrice ={this.getPrice} currentProduct={this.state.currentProduct} selected={this.state.selected}/>)}  />
                <Route path="/product/:link" exact render={(props) => (<Product addcart={this.addToCart} categories = {this.state.categories} handleChange ={this.handleChange} getPrice ={this.getPrice} currentProduct={this.state.currentProduct} selected={this.state.selected}/>)}  />
                <Route path="/checkout" exact render={(props) => (<Checkout clearCart = {this.clearCart}  logout = {this.logout} cart={this.state.cart} cartcount={this.getCartCount()} removeFromCart={this.removeFromCart} user={this.state.user}/>)}  />
                <Route path="/vieworder/:link" exact render={(props) => (<ViewOrder user={this.state.user}/>)}  />
                <Route path="/myaddress" exact render={(props) => (<MyAddress user={this.state.user}  logout={this.logout} />)} />
                <Route path="/about-us" exact render={(props) => (<About/>)}  />
                <Route path="/contact-us" exact render={(props) => (<Contact contact={this.contact}/>)}  />
          </div>
      </Router>
    );
  }
}

export default App