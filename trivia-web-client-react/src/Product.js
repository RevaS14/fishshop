import React, { Component } from 'react';
import { API_BASE_URL, BASE_URL ,DEV_BASE_URL} from './config';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
import RelatedProduct from './components/Product.js';

class Product extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: null,
            isLoading: null,
            images: null,
            related: null,
            product_name : window.location.pathname.replace('/product/','')
        };
    }
   
    componentDidMount() {
        this.getPlayers();
        this.getImages();
       
    }

    async getPlayers() {
        if (! this.state.products) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/product/'+this.state.product_name);
                const playersList = await response.json();
                this.setState({ products: playersList, isLoading: false});
                this.getRelated();
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }
    async getRelated() {
        if (! this.state.related) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/related/'+this.state.products.data[0].ProductId);
                const relatedList = await response.json();
                this.setState({ related: relatedList, isLoading: false});
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }
    async getImages() {
        if (! this.state.images) {
            try {
                this.setState({ isLoading: true });
                const response = await fetch(API_BASE_URL + '/productImages/'+this.state.product_name);
                const playersList = await response.json();
                this.setState({ images: playersList, isLoading: false});
            } catch (err) {
                this.setState({ isLoading: false });
                console.error(err);
            }
        }
    }

    render() {
       
        return (
            <div>
                <div class="mad-breadcrumb with-bg title-center show-title-1">
                    <div class="container">
                        <h1 class="mad-page-title breadcrumb-title">{this.state.products && this.state.products.data[0].ProductName}</h1>					
                    </div>
                </div>
<div class="mad-content no-pd bg-gray">
      <div class="container">
        <div class="mad-section pad-bottom-3rem mad-section--stretched mad-colorizer--scheme-">
          <div class="mad-colorizer-bg-color">
            <div class="with-svg-item svg-right-side"><img src="images/top_half_right.png" alt=""/></div>
          </div>
          <div class="row vr-size-2 hr-size-1">
            <main id="main" class="col-xl-8">
              
              <div class="mad-products mad-product-single">
                <div class="image-preview-container">
                  <div class="image-preview">
                  {this.state.images && 
                    <ImageGallery items={this.state.images} />
                    }
                  </div>
                </div>

              </div>
              </main>
              <aside id="sidebar" class="col-xl-4 mad-sidebar">
              <h2 class="mad-product-title">{this.state.products && this.state.products.data[0].ProductName}</h2>
              {this.state.products &&
                <div>
                <div class="mad-product-description">
                  <p>{this.state.products.data[0].ProductDescription}</p>
                </div>
                <div class="mad-product-calc">
                  <span class="mad-product-price"><i>MRP:</i> ₹{this.props.selected && this.props.currentProduct==this.state.products.data[0].ProductId ? this.props.selected : this.state.products.data[0].Price }</span>
                  <div class="mad-calc">
                    <div class="quantity">
                    {this.state.products && this.state.products.variations[0].length > 0 && 
                              <select class="form-control" id={this.state.products.data[0].ProductId} value={this.props.selected && this.props.currentProduct==this.state.products.data[0].ProductId ? this.props.selected : this.state.products.data[0].Price} onChange={this.props.handleChange}>
                                {this.state.products.variations[0].map((variation, vindex) =>
                                 <option value={variation.Price}>{variation.Size}</option>
                                  )}
                                </select> 
                                }
                    </div>
                    {this.state.products.data[0].Stock ? (
                    <button class="add-to-cart addCartBtn-home" data-cat="
                              
                              Fish &amp; Seafood
                            " data-amt="235" onClick={() =>
                              this.props.addcart({
                                id: this.state.products.data[0].ProductId,
                                name: this.state.products.data[0].ProductName,
                                product: this.state.products.data[0],
                                amount: this.props.currentProduct==this.state.products.data[0].ProductId ? this.props.selected : this.state.products.data[0].Price 
                              })
                            }>Add To Cart</button>
                            ) : (
                              <button class="add-to-cart addCartBtn-home">Out Of Stock</button>
                      )}
                  </div>
                </div>
                </div>
                }

              <div class="mad-product-footer">
                  <div class="mad-share-wrap"><span>Share:</span>
                    <div class="mad-social-icons style-3 size-2">
                      <ul>
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-tumblr"></i></a></li>
                        <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              
              
            </aside>
              </div>
            </div>
            <hr></hr>
            </div>
            </div>
            
            <div class="mad-section pad-top-0 mad-section--stretched mad-colorizer--scheme- explore our_products">
   
          <div class="mad-colorizer-bg-color" >
            <div class="with-svg-item bottom"><img src={ BASE_URL +'assets/images/fish_left.png'} alt="" /></div>
          </div>
          <div className="container">
          <h4>You Might Also Like</h4>
          <div class="row">
          {!this.state.related || Object.keys(this.state.related.data).length===0 ? <h3>No Products Found</h3> : null}
          {this.state.related && this.state.related.data.length > 0 && this.state.related.data.map((product, index) =>
                
                    <RelatedProduct
                    key={product.id}
                    product={product} index={0} addtocart={this.props.addcart} selected ={this.props.selected} handleChange ={this.props.handleChange} getPrice ={this.props.getPrice} currentProduct={this.props.currentProduct} variations={this.state.products.variations[0]} />
                
            )}

          </div>
          </div>
          {this.state.related && <div>
          <div class="bubbles bubble-1"></div>
          <div class="bubbles bubble-5"></div>
          <div class="bubbles bubble-2"></div>
          <div class="bubbles bubble-6"></div>
          <div class="bubbles bubble-3"></div>
          <div class="bubbles bubble-7"></div>
          <div class="bubbles bubble-4"></div>
          <div class="bubbles bubble-8"></div>
          </div>
          }
        </div>
            </div>
        );
    }

}

export default Product