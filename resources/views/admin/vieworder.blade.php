
@extends('layouts.default')
@section('content')
  
<div class="content-wrapper" style="min-height: 1602px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Order Details</h1>
          </div>
          <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
              <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/orders') }}">Go Back</a>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
      @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success">
        <i class="fas fa-check"></i> 
        {{ Session::get('success') }}
        </div>
        @endif
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">

            <div class="row">
                <div class="col-12">
                  <h4>
                    Order No: {{ $order->OrderNo }} 
                    <span class="badge badge--<?php if($order->Status=='C') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($order->Status=='C') { echo 'Delivered'; } else { echo 'Pending'; } ?></span> 
                    <small class="float-right">Date: {{ date('d-m-Y',strtotime($order->created_at)) }} </small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>{{ $customer->UserName }}</strong><br>
                    {{ $customer->Address }}<br>
                   
                    Phone: {{ $customer->Mobile }}<br>
                    Email: {{ $customer->Email }}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To 
                  <address>
                    <strong>{{ $shipping->Name }} </strong><br>
                    {{ $shipping->Address }}<br>
                    Phone: {{ $shipping->Mobile }}<br>
                    Email: {{ $shipping->Email }}
                  </address>
                </div>
                <!-- /.col -->
               
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Product Name</th>
                      <th>Image</th>
                      <th>Weight</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $total =0 ; ?>
                    @foreach($orders as $row)
                    <tr>
                      <td>{{ $row->ProductName }}</td>
                      <td> <img width="100" src="{{ asset($row->ImagePath) }}"> </td>
                      <td>{{ $row->Weight }}</td>
                      <td>{{ $row->Qty }}</td>
                      
                      <td>Rs. {{ $row->Price }}</td>
                      <td>Rs. {{ $row->Qty*$row->Price }}</td>
                      <?php $total = $total + $row->Qty*$row->Price; ?>
                    </tr>
                    @endforeach
                    
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-8">
                 
                </div>
                <!-- /.col -->
                <div class="col-4">

                  <div class="table-responsive">
                    <table class="table">
                      <tbody><tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>Rs. {{ $total }}</td>
                      </tr>
                      <tr>
                        <th>Tax (9.3%)</th>
                        <td>Rs. 20</td>
                      </tr>
                      <tr>
                        <th>Delivery Charge:</th>
                        <td>Rs. 50</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>Rs. {{ $total+20+50 }}</td>
                      </tr>
                    </tbody></table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                 
                  <a target="_blank" href="{{ url('/print-invoice/'.$order->OrderId) }}" class="btn btn-primary float-left" style="margin-right: 5px;">
                    <i class="fas fa-print"></i> Print
                  </a>
                  <?php if($order->Status=='P') { ?>
                  <a href="{{ url('/complete-order/'.$customer->Email, $order->OrderId) }}" class="btn btn-success float-right" style="margin-right: 5px;">
                    <i class="far fa-credit-card"></i> Mark as Completed
                  </a>
                  <?php } ?>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @stop



