
@extends('layouts.default')
@section('content')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">

      <div class="row align-items-center mb-30 justify-content-between">
    <div class="col-lg-6 col-sm-6">
        <h6 class="page-title">Orders</h6>
    </div>
    
   
</div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success">
        <i class="fas fa-check"></i> 
        {{ Session::get('success') }}
        </div>
        @endif

        <!-- SELECT2 EXAMPLE -->

         
          <table id="example2" class="table table-hover">
                  <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>User Name</th>
                    <th>Total Items</th>
                    <th>Total</th>
                    <th>Payment Method</th>
                    <th>Status</th>
                    <th>Created On</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $row)
                  <tr>
                    <td> {{ $row->OrderNo }}</td>
                   
                    <td> <a href="{{ url('/view-user/'.$row->UserId) }}">{{ $row->UserName }}</a></td>
                    <td> {{ $controller->getTotalItems($row->OrderId) }}</td>
                    <td> {{ $row->Total }}</td>
                    <td> <?php if($row->PaymentMethod=='C') { echo 'Cash On Delivery'; } ?></td>
                    <td> <span class="badge badge--<?php if($row->Status=='C') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($row->Status=='C') { echo 'Delivered'; } else { echo 'Pending'; } ?></span>  </td>
                    <td> {{ date('d-m-Y',strtotime($row->created_at)) }}</td>
                    <td> 
                    <a class="icon-btn" href="{{ url('/view-order/'.$row->OrderId)}}"> <i class="fas fa-eye"></i></a>
                    <a target="_blank" class="icon-btn" href="{{ url('/print-invoice/'.$row->OrderId) }}">
                    <i class="fas fa-print"></i> 
                  </a>
                  </td>
                  </tr>
                  @endforeach
                  </tbody>
          </table>
            
         
        <!-- SELECT2 EXAMPLE -->
    

        
        
        <!-- /.row -->
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @stop



