
@extends('layouts.default')
@section('content')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">

      <div class="row align-items-center mb-30 justify-content-between">
    <div class="col-lg-6 col-sm-6">
        <h6 class="page-title">Delivery Locations</h6>
    </div>
    <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
            <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/add-location') }}"><i class="fa fa-fw fa-plus"></i>Add New</a>
    </div>
</div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('error'))
        <div class="alert alert-danger">
        <i class="fas fa-ban"></i> 
        {{ Session::get('error') }}
        </div>
        @endif

        @if(Session::has('success'))
        <div class="alert alert-success">
        <i class="fas fa-check"></i> 
        {{ Session::get('success') }}
        </div>
        @endif

        <!-- SELECT2 EXAMPLE -->

         
          <table id="example2" class="table table-hover">
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Pincode</th>
                    <th>Status</th>
                    <th>Created On</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($data as $row)
                  <tr>
                    <td> {{ $row->name }}</td>
                    <td> {{ $row->pincode }}</td>
                    <td> <span class="badge badge--<?php if($row->status=='A') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($row->status=='A') { echo 'Active'; } else { echo 'Deactive'; } ?></span>  </td>
                    <td> {{ date('d-m-Y',strtotime($row->created_at)) }}</td>
                    <td> <a class="icon-btn" href="{{ url('/edit-location/'.$row->id)}}"> <i class="fas fa-pen"></i></a>
                    <a class="icon-btn delete" data-id="{{ $row->id }}" data-table="Location"> <i class="fas fa-trash"></i></a></td>
                  </tr>
                  @endforeach
                  </tbody>
          </table>
            
         
        <!-- SELECT2 EXAMPLE -->
    

        
        
        <!-- /.row -->
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @stop



