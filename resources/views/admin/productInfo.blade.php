
@extends('layouts.default')
@section('content')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <div class="row align-items-center mb-30 justify-content-between">
        <div class="col-lg-6 col-sm-6">
            <h6 class="page-title">Add Product</h6>
        </div>
        <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
              <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/products') }}">Go Back</a>
        </div>
      </div>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <div class="card card-default">
 
          <!-- /.card-header -->
          <div class="card-body">
          <div class="col-md-8">
            <form class="form-horizontal" action="{{ url('/manage-product')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="productId" value="<?php if($data) { echo $data->ProductId; } ?>">
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Product Name</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control form-control-border" name="productName" id="inputEmail3" required placeholder="Product Name" value="<?php if($data) { echo $data->ProductName; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Description</label>
                    <div class="col-sm-10">
                      <textarea id="summernote" class="form-control form-control-border" name="description" required placeholder="Description"> <?php if($data) { echo $data->ProductDescription; } ?></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-7">
                    <select class="custom-select form-control-border" name="category" id="exampleSelectBorder">
                    <option value="">Select</option>
                    @foreach($categories as $row)
                    <option <?php if($data) { if($data->Category==$row->CategoryId) { echo 'selected'; } } ?> value="{{ $row->CategoryId }}">{{ $row->CategoryName }}</option>
                    @endforeach
                  </select>
                    </div>
                  </div>

                  <div class="clone">

                  
                  @if($data && $options) @php $i = 1; @endphp @foreach($options as $row)
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Options <span class="required">*</span></label>
                      <div class="col-sm-7">
                        <div class="row">
                          <div class="col-sm-3">
                          <input class="form-control form-control-border" value="{{ $row->Size }}" placeholder="Weight" name="weight[]" required id="ProductForm_option" type="text">                <div class="errorMessage" id="ProductForm_option_em_" style="display:none"></div>              </div>
                        
                          <div class="col-sm-3">
                            <input class="form-control form-control-border" value="{{ $row->Price }}" placeholder="Price" name="price[]" required id="ProductForm_mrp_price" type="text">                <div class="errorMessage" id="ProductForm_mrp_price_em_" style="display:none"></div>              
                          </div>

                          <div class="col-sm-3">
                          <select class="custom-select form-control-border" name="option_status[]" id="exampleSelectBorder">
                            <option value="A" <?php if($row->Status=='A') { echo 'selected'; } ?>>Active</option>
                            <option value="D" <?php if($row->Status=='D') { echo 'selected'; } ?>>Deactive</option>
                          </select>
                          </div>

                          <div class="col-sm-1">
                            <?php if($i != 1 ) { ?> 
                            <input type="button" class="btn btn-danger removeclone" value="-">
                            <?php } else { ?>
                            <input type="button" class="btn btn-primary addclone" value="+">
                            <?php } ?>
                          </div>
                        </div>
                      </div>
                  </div>
                  @php $i++ @endphp @endforeach
                  @else
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Options <span class="required">*</span></label>
                      <div class="col-sm-7">
                        <div class="row">
                          <div class="col-sm-3">
                          <input class="form-control form-control-border" value="" placeholder="Weight" name="weight[]" required id="ProductForm_option" type="text">                <div class="errorMessage" id="ProductForm_option_em_" style="display:none"></div>              </div>
                        
                          <div class="col-sm-3">
                            <input class="form-control form-control-border" value="" placeholder="Price" name="price[]" required id="ProductForm_mrp_price" type="text">                <div class="errorMessage" id="ProductForm_mrp_price_em_" style="display:none"></div>              
                          </div>

                          <div class="col-sm-3">
                          <select class="custom-select form-control-border" name="option_status[]" id="exampleSelectBorder">
                            <option value="A" <?php if($data) { if($data->Status=='A') { echo 'selected'; }} ?>>Active</option>
                            <option value="D" <?php if($data) { if($data->Status=='D') { echo 'selected'; }} ?>>Deactive</option>
                          </select>
                          </div>

                          <div class="col-sm-1">
                            <input type="button" class="btn btn-primary addclone" value="+">
                          </div>
                        </div>
                      </div>
                  </div>
                  @endif
                </div>

                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-7">
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="image[]" class="custom-file-input form-control-border" multiple <?php if(!$data) { echo 'required'; } ?> id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    
                    </div>                    
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Stock</label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control form-control-border" name="stock" id="inputEmail3" required placeholder="Stock" value="<?php if($data) { echo $data->Stock; } ?>">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Discount</label>
                    <div class="col-sm-7">
                      <input type="number" class="form-control form-control-border" name="discount" id="inputEmail3" required placeholder="Discount" value="<?php if($data) { echo $data->Discount; } ?>">
                    </div>
                  </div>
                  @if($data && $images) @foreach($images as $row)
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Existing Image</label>
                    <div class="col-sm-7">
                    <img width="100" src="{{ asset($row->ImagePath) }}">
                    </div>
                  </div>
                  @endforeach
                  @endif
                  <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-7">
                    <select class="custom-select form-control-border" name="status" id="exampleSelectBorder">
                    <option value="A" <?php if($data) { if($data->Status=='A') { echo 'selected'; }} ?>>Active</option>
                    <option value="D" <?php if($data) { if($data->Status=='D') { echo 'selected'; }} ?>>Deactive</option>
                  </select>
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <div class="offset-sm-2 col-sm-10">
                      <div class="">
                      <button type="submit" class="btn btn-sm btn--primary box--shadow1">Submit</button>   
                      </div>
                    </div>
                  </div>
                <!-- /.card-body -->
               
              </form>
              </div>

            
          </div>
          <!-- /.card-body -->
          <div class="card-footer">
           
          </div>
        </div>
        <!-- /.card -->

        <!-- SELECT2 EXAMPLE -->
    

        
        
        <!-- /.row -->
        
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  @stop



