
@extends('layouts.default')
@section('content')
  
<div class="content-wrapper" style="min-height: 1602px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Details</h1>
          </div>
          <div class="col-lg-6 col-sm-6 text-sm-right mt-sm-0 mt-3 right-part">
              <a class="btn btn-sm btn--primary box--shadow1 text--small addBtn" href="{{ url('/users') }}">Go Back</a>
        </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- About Me Box -->
            <div class="card card-primary border-none">
              <div class="card-header border-none">
                <h3 class="card-title">About</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              

                <p class="text-muted">
                <strong><i class="fas fa-book mr-1"></i> Name</strong> : <?php echo $user->UserName; ?>
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                <p class="text-muted"><?php echo $user->Address; ?></p>

                <hr>

               

                <p class="text-muted">  <strong><i class="far fa-file-alt mr-1"></i> Total Orders</strong> : {{ count($orders) }}</p>
                <hr>

                

                <p class="text-muted">
                <strong><i class="far fa-file-alt mr-1"></i> Status</strong> : <span class="badge badge--<?php if($user->Status=='A') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($user->Status=='A') { echo 'Active'; } else { echo 'Deactive'; } ?></span>
                </p>
                <hr>

               

                <p class="text-muted">
                <strong><i class="far fa-file-alt mr-1"></i> Registered On</strong> : {{ date('d-m-Y',strtotime($user->created_at)) }}
                </p>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            
            <table id="example2" class="table table-hover">
                  <thead>
                  <tr>
                    <th>Order ID</th>
                    <th>User Name</th>
                    <th>Total Items</th>
                    <th>Total</th>
                    <th>Payment Method</th>
                    <th>Status</th>
                    <th>Created On</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach($orders as $row)
                  <tr>
                    <td> {{ $row->OrderNo }}</td>
                   
                    <td> {{ $user->UserName }}</td>
                    <td> {{ $controller->getTotalItems($row->OrderId) }}</td>
                    <td> {{ $row->Total }}</td>
                    <td> <?php if($row->PaymentMethod=='C') { echo 'Cash On Delivery'; } ?></td>
                    <td> <span class="badge badge--<?php if($row->Status=='C') { echo 'success'; } else { echo 'warning'; } ?>"><?php if($row->Status=='C') { echo 'Delivered'; } else { echo 'Pending'; } ?></span>  </td>
                    <td> {{ date('d-m-Y',strtotime($row->created_at)) }}</td>
                    <td> 
                    <a class="icon-btn" href="{{ url('/view-order/'.$row->OrderId)}}"> <i class="fas fa-eye"></i></a>
                    <a target="_blank" class="icon-btn" href="{{ url('/print-invoice/'.$row->OrderId) }}">
                    <i class="fas fa-print"></i> 
                  </a>
                  </td>
                  </tr>
                  @endforeach
                  </tbody>
          </table>
  
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

  @stop



