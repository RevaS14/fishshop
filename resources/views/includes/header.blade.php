<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      
     
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-bell text--primary" style="font-size: 25px;"></i>
          <span class="badge badge-warning navbar-badge">15</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">15 Notifications</span>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-envelope mr-2"></i> 4 new messages
            <span class="float-right text-muted text-sm">3 mins</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-users mr-2"></i> 8 friend requests
            <span class="float-right text-muted text-sm">12 hours</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="fas fa-file mr-2"></i> 3 new reports
            <span class="float-right text-muted text-sm">2 days</span>
          </a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
     
      <li class="nav-item dropdown">
                <button type="button" class="dropdown-btn" data-toggle="dropdown" data-display="static" aria-haspopup="true" aria-expanded="false">
            <span class="navbar-user">
              <span class="navbar-user__thumb"><img src="https://script.viserlab.com/trafficlab/assets/admin/images/profile/60d1d8fdaa87e1624365309.png" alt="image"></span>

              <span class="navbar-user__info">
                <span class="navbar-user__name">Admin</span>
              </span>
              <span class="icon"><i class="las la-chevron-circle-down"></i></span>
            </span>
                </button>
                <div class="dropdown-menu dropdown-menu--sm p-0 border-0 box--shadow1 dropdown-menu-right">
                    <a href="https://script.viserlab.com/trafficlab/admin/profile" class="dropdown-menu__item d-flex align-items-center px-3 py-2">
                        <i class="dropdown-menu__icon fas fa-user-circle"></i>
                        <span class="dropdown-menu__caption">Profile</span>
                    </a>

                    <a href="https://script.viserlab.com/trafficlab/admin/password" class="dropdown-menu__item d-flex align-items-center px-3 py-2">
                        <i class="dropdown-menu__icon fas fa-key"></i>
                        <span class="dropdown-menu__caption">Password</span>
                    </a>

                    <a href="https://script.viserlab.com/trafficlab/admin/logout" class="dropdown-menu__item d-flex align-items-center px-3 py-2">
                        <i class="dropdown-menu__icon fas fa-sign-out-alt"></i>
                        <span class="dropdown-menu__caption">Logout</span>
                    </a>
                </div>
            </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4 sidebar capsule--rounded bg_img overlay--indigo" data-background="https://script.viserlab.com/trafficlab/assets/admin/images/sidebar/2.jpg" style="background-image: url('https://script.viserlab.com/trafficlab/assets/admin/images/sidebar/2.jpg');"> 
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <span class="brand-text font-weight-light">Admin</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="sidebar__menu nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/dashboard')}}" class="nav-link {{ (request()->is('dashboard')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Dashboard
              </p>
            </a>
            
          </li>
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/categories')}}" class="nav-link {{ (request()->is('categories*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Categories
              </p>
            </a>
          </li>
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/locations')}}" class="nav-link {{ (request()->is('locations*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-map-marker"></i>
              <p>
                Delivery Locations
              </p>
            </a>
          </li>
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/banners')}}" class="nav-link {{ (request()->is('banners*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-image"></i>
              <p>
                Banners
              </p>
            </a>
          </li>
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/products')}}" class="nav-link {{ (request()->is('products*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Products
              </p>
            </a>
          </li>
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/orders')}}" class="nav-link {{ (request()->is('orders*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-shopping-basket"></i>
              <p>
                Orders
              </p>
            </a>
          </li>
          <li class="nav-item sidebar-menu-item">
            <a href="{{ url('/users')}}" class="nav-link {{ (request()->is('users*')) ? 'active' : '' }}">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Users
              </p>
            </a>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>