
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <title>Neopolitan Invoice Email</title>
  <!-- Designed by https://github.com/kaytcat -->
  <!-- Robot header image designed by Freepik.com -->

  <style type="text/css">
  @import url(http://fonts.googleapis.com/css?family=Droid+Sans);

  /* Take care of image borders and formatting */

  img {
    max-width: 600px;
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
  }

  a {
    text-decoration: none;
    border: 0;
    outline: none;
    color: #bbbbbb;
  }

  a img {
    border: none;
  }

  /* General styling */

  td, h1, h2, h3  {
    font-family: Helvetica, Arial, sans-serif;
    font-weight: 400;
  }

  td {
    text-align: center;
  }

  body {
    -webkit-font-smoothing:antialiased;
    -webkit-text-size-adjust:none;
    width: 100%;
    height: 100%;
    color: #37302d;
    background: #ffffff;
    font-size: 16px;
  }

   table {
    border-collapse: collapse !important;
  }

  .headline {
    color: #ffffff;
    font-size: 36px;
  }

 .force-full-width {
  width: 100% !important;
 }

 .force-width-80 {
  width: 80% !important;
 }

 .order_table
 {
    margin-top: 30px;
 }
.order_table thead tr th {
    color:#ffffff; background-color:#ac4d2f; padding: 10px 0px;
}

.order_table tbody tr td {
    color:#933f24; padding:10px 0px; background-color: #f7a084;
}

  </style>

  <style type="text/css" media="screen">
      @media screen {
         /*Thanks Outlook 2013! http://goo.gl/XLxpyl*/
        td, h1, h2, h3 {
          font-family: 'Droid Sans', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
        }
      }
  </style>

  <style type="text/css" media="only screen and (max-width: 480px)">
    /* Mobile styles */
    @media only screen and (max-width: 480px) {

      table[class="w320"] {
        width: 320px !important;
      }

      td[class="mobile-block"] {
        width: 100% !important;
        display: block !important;
      }


    }
  </style>
</head>
<body class="body" style="padding:0; margin:0; display:block; background:#ffffff; -webkit-text-size-adjust:none" bgcolor="#ffffff">
<table align="center" cellpadding="0" cellspacing="0" class="force-full-width" height="100%" >
  <tr>
    <td align="center" valign="top" bgcolor="#ffffff"  width="100%">
      <center>
        <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" width="600" class="w320">
          <tr>
            <td align="center" valign="top">

                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#4dbfbf">
                  <tr>
                    <td>
                    <br>
                      <img src="http://test.thathafishshop.com/assets/images/logo1.png" width="150" alt="Thatha FishShop">
                    </td>
                  </tr>
                  
                
                </table>

                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#f5774e">
                  <tr>
                    <td style="background-color:#f5774e;">

                    <center>
                      <table style="margin:0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
                        <tr>
                          <td style="text-align:left; color:#933f24">
                          <br>
                          <br>
                          <h2>Shipping Address</h2>
                            <span style="color:#ffffff;">{{ $shipping->Name }}</span> <br>
                            {{ $shipping->Address }} <br>
                            {{ $shipping->Mobile }}
                          </td>
                          <td style="text-align:right; vertical-align:top; color:#933f24">
                          <br>
                          <br>
                            <span style="color:#ffffff;">Invoice: {{ $order->OrderNo }}</span> <br>
                            {{ date('M d, Y',strtotime($order->created_at)) }}
                          </td>
                        </tr>
                      </table>


                      <table  cellspacing="0" cellpadding="0" class="order_table force-width-80">
                        <thead>
                            <th>Product Name</th>
                            <th>Weight</th>
                            <th>Price</th>
                            <th>Total</th>
                        </thead>
                        <tbody>
                        <div style="display: none">{{ $total=0 }}</div?
                        @foreach($items as $item)
                            <tr>
                                <td>{{ $item->ProductName }}</td>
                                <td>{{ $item->Weight }}</td>
                                <td>Rs. {{ $item->Price }}</td>
                                <div style="display: none">{{$total += $item->Price}}</div>
                                 <td>Rs. {{ $item->Price }} </td>
                            </tr>
                           
                        @endforeach
                            <tr>
                                <td> Sub Total</td>
                                <td colspan="">Rs. {{ $total }}</td> 
                            </tr>
                            <tr>
                                <td> Delivery Charge</td>
                                <td colspan="">Rs. 50</td> 
                            </tr>
                            <tr>
                                <td> Tax</td>
                                <td colspan="">Rs. 20</td> 
                            </tr>
                            <tr>
                                <td> Grand Total</td>
                                <td colspan="">{{ $total+50+20 }}</td> 
                            </tr>
                        </tbody>
                      </table>


                      <table  style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
                        <tr>
                          <td style="color:#933f24; text-align:left; border-bottom:1px solid #933f24;">
                          <br>
                          <br>
                            The amount of Rs. {{ $total+50+20 }} has been calculated for this order.
                            <br>
                            <br>
                          </td>
                        </tr>
                      </table>


                      <table style="margin: 0 auto;" cellspacing="0" cellpadding="0" class="force-width-80">
                        <tr>
                          <td style="text-align:left; color:#933f24;">
                          <br>
                            Thank you for your business. Please <a style="color:#ffffff;" href="#">contact us</a> with any questions regarding this invoice.
                          <br>
                          <br>
                          Thatha FishShop
                          <br>
                          <br>
                          <br>
                          </td>
                        </tr>
                      </table>
                    </center>



                    </td>
                  </tr>


                </table>

                <table style="margin: 0 auto;" cellpadding="0" cellspacing="0" class="force-full-width" bgcolor="#414141" style="margin: 0 auto">
                  <tr>
                    <td style="background-color:#414141;">
                    <br>
                    <br>
                      <img src="https://www.filepicker.io/api/file/R4VBTe2UQeGdAlM7KDc4" alt="google+">
                      <img src="https://www.filepicker.io/api/file/cvmSPOdlRaWQZnKFnBGt" alt="facebook">
                      <img src="https://www.filepicker.io/api/file/Gvu32apSQDqLMb40pvYe" alt="twitter">
                      <br>
                      <br>
                    </td>
                  </tr>
                  <tr>
                    <td style="color:#bbbbbb; font-size:12px;">
                      <a href="#">View in browser</a> | <a href="#">Unsubscribe</a> | <a href="#">Contact</a>
                      <br><br>
                    </td>
                  </tr>
                  <tr>
                    <td style="color:#bbbbbb; font-size:12px;">
                       © 2021 All Rights Reserved
                       <br>
                       <br>
                    </td>
                  </tr>
                </table>





            </td>
          </tr>
        </table>
    </center>
    </td>
  </tr>
</table>
</body>
</html>