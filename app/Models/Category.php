<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    protected $primaryKey = 'CategoryId';
    protected $fillable = ['CategoryName','CategoryLink','CategoryImage','Status','CreatedOn'];
    use HasFactory;

    public static function getCategories(){
        $value=DB::table('categories')->orderBy('CategoryId', 'asc')->get();
        return $value;
    }
}
