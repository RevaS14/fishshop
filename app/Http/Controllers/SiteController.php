<?php

namespace App\Http\Controllers;
use App\Models\Location;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ShippingDetail;
use App\Models\User;
use App\Models\Product;
use Request;
use Mail;
use Hash;

class SiteController extends Controller
{
    public function getLocations() {
        $data = Location::select("*")
        ->where("Status", "=", "A")->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row; 
        }
        return $arr;
    }

    public function Signup(Request $request) {


        $request = Request::instance();
        $name = $request->input('username');
        $email =  $request->input('email');
        $mobile = $request->input('mobile');
        $address =  $request->input('address');
        $password =  $request->input('password');

        $emailExists = User::select("*")
        ->where("Email", "=", $email)->first();

        if($emailExists) {
            return array("Email");
            exit;
        }

        $mobileExists = User::select("*")
        ->where("Mobile", "=", $mobile)->first();
        
        if($mobileExists) {
            return array("Mobile");
            exit;
        }

        $order = new User;
        $order->UserName = $name;
        $order->Mobile = $mobile;
        $order->Email = $email;
        $order->Address = $address;
        $order->Password = Hash::make($password);
        $order->Status = 'A';
        $order->save();

        $user =  User::select("*")->where("Mobile", "=", $request->input('mobile'))->first();

        if($user) {
            return $user;
        } else {
            return array('Not found');
        }
    }

    public function UserOrders($id) {
        $data = Order::select('OrderId','OrderNo','Total','PaymentMethod','Status','created_at')
        ->where("UserId", "=", $id)->orderBy('OrderId', 'DESC')->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row;         
        }
        return json_encode($arr);
    }

    public function ViewOrder($id) {
        $data = Product::select("products.ProductName","products.ProductLink","product_images.ImagePath","product_variations.Size","order_details.Price")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->join('order_details','order_details.ProductId','=','products.ProductId')
        ->where("order_details.OrderId", "=", $id)
        ->groupBy('product_variations.ProductId')
        ->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row;         
        }
        return json_encode($arr);
    }

    public function getShipping($id) {
        $data = ShippingDetail::select("*")
        ->where("OrderId", "=", $id)
        ->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row;         
        }
        return $arr;
    }

    public function getOrder($id) {
        $data = Order::select("*")
        ->where("OrderId", "=", $id)
        ->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row;         
        }
        return $arr;
    }

    public function getUser($id) {
        $data = User::select("*")
        ->where("UserId", "=", $id)
        ->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row;         
        }
        return $arr;
    }

    public function UpdateAddress(Request $request) {
        $request = Request::instance();
        $name = $request->input('name');
        $phone = $request->input('phone');
        $address =  $request->input('address');
        $user_id = $request->input('user_id');

        $single_order = User::findOrFail($user_id);
        $single_order->UserName = $name;
        $single_order->Mobile = $phone;
        $single_order->Address = $address;
        $single_order->save();
        return array("status"=>true);

    }

    public function Orders(Request $request) {
        $request = Request::instance();
        $name = $request->input('name');
        $email =  $request->input('email');
        $phone = $request->input('phone');
        $address =  $request->input('address');
        $notes =  $request->input('notes');
        $pincode =  $request->input('pincode');
        $user_id = $request->input('user_id');
        $cart = $request->input('cart');
        $location =  $request->input('location');
        $total = 0;
        foreach($cart as $row) {
            $total = $total + $row['amount'];
        }
       
        $order = new Order;
        $order->UserId = $user_id;
        $order->Notes = $notes;
        $order->Total = $total;
        $order->save();
        $id = $order->OrderId;

        $shipping = new ShippingDetail;
        $shipping->UserId = $user_id;
        $shipping->OrderId = $id;
        $shipping->Name = $name;
        $shipping->Email = $email;
        $shipping->Mobile = $phone;
        $shipping->Address = $address;
        $shipping->Pincode = $pincode;
        $shipping->Location = $location;
        $shipping->save();
        $shipping_id = $shipping->id;

        foreach($cart as $row) {
            $details = new OrderDetail;
            $details->UserId = $user_id;
            $details->OrderId = $id;
            $details->ProductId = $row['product']['ProductId'];
            $details->ProductVariationId = $row['product']['ProductVariationId'];
            $details->Qty = 1;
            $details->Weight = $row['product']['Size'];
            $details->Price = $row['product']['Price'];
            $details->save();
        }
        $orderNo = 'TF/'.date('y').'/'.date('m').'/'.date('d').'/'.$id;
        $single_order = Order::findOrFail($id);
        $single_order->ShippingId = $shipping_id;
        $single_order->OrderNo = $orderNo;
        $single_order->save();

        $this->sendMail($email,$id);

        return array("status"=>true);

    }

    public function sendMail($email,$order_id) {
        
        $items = OrderDetail::select("*")
        ->join('products','order_details.ProductId','=','products.ProductId')
        ->where("OrderId", "=", $order_id)->get();

        $order = Order::select("*")
        ->where("OrderId", "=", $order_id)->first();

        $shipping = ShippingDetail::select("*")
        ->where("OrderId", "=", $order_id)->first();

        $data = array('email'=>$email,'items'=>$items,'order'=>$order,'shipping'=>$shipping);
        Mail::send('mail.order', $data, function($message) use ($email) {
           $message->to($email)->subject
              ('New Order from Thatha FishShop');
           $message->from('info@thathafishshop.com','Thatha FishShop');
        });
        //echo "HTML Email Sent. Check your inbox.";
     }

     public function contact(Request $request) {

        $request = Request::instance();
        $name = $request->input('name');
        $mobile = $request->input('mobile');
        $email =  $request->input('email');
        $message =  $request->input('message');
        
        $data = array('email'=>$email,'name'=>$name,'mobile'=>$mobile,'message'=>$message);
        Mail::send('mail.enquiry', $data, function($message) use ($email) {
           $message->to('info@thathafishshop.com')->subject
              ('New Enquiry from Thatha FishShop');
           $message->from($email,$name);
        });
        //echo "HTML Email Sent. Check your inbox.";
     }
}
