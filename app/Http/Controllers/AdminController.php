<?php

namespace App\Http\Controllers;

use Request;
use App\Models\Category;
use App\Models\User;
use App\Models\Slider;
use App\Models\Product;
use App\Models\ProductVariation;
use App\Models\ProductImage;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ShippingDetail;
use App\Models\Location;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use Hash;
use Session;
use Storage;

class AdminController extends Controller
{
    public function index() {
        if(Session::get('user_id')){
            return redirect('/dashboard');
        }
        return view('admin.index');
    }

    public function login(Request $request) {
        $request = Request::instance();
        $email = $request->input('email');
        $user =  User::select("*")->where("Email", "=", $email)->first();
        if($user){
            if($user->UserType=='A'){
                if (Hash::check($request->input('password'), $user->Password)) {
                    Session::put('user_id', $user->UserId);
                    return redirect('/dashboard');
                } else {
                    Session::flash('error', 'Invalid Credentials!'); 
                }
            } else {
                Session::flash('error', 'Invalid Access!'); 
            }
        } else {
            Session::flash('error', 'User not found!'); 
        }
        return redirect('/');


        return view('admin.index');
    }

    public function dashboard() {
        $this->securePage();
        return view('admin.dashboard');
    }

    public function categories() {
        $this->securePage();
        $data['data'] = Category::getCategories();
        return view('admin.categories')->with($data);
    }

    public function addCategory() {
        $data['data'] = '';
        return view('admin.categoryInfo')->with($data);
    }

    public function editCategory($id) {
        $data['data'] = Category::find($id);
        return view('admin.categoryInfo')->with($data);
    }

    public function manageCategory(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('categoryId');
        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'categoryName' => 'string|max:40',
                    'image' => 'mimes:jpeg,jpg,png|max:2000',
                ]);
                $extension = $request->image->extension();
                $fileName = 'category'.time();
                $name = $fileName.'.'.$extension;
                $request->file('image')->move(public_path().'/assets/images', $name);  
                $url = 'public/assets/images/'.$name;
            }
        }
        if($id) {
            $post = Category::find($id);
            $post->CategoryName = $request->input('categoryName');
            $post->CategoryLink = strtolower(str_replace("-"," ",$request->input('categoryName')));
            if($url) {
                $post->CategoryImage = $url;
            }
            $post->Status = $request->input('status');
            $post->save();
            Session::flash('success', 'Successfully Updated!'); 
        } else {
            $file = Category::create([
                'CategoryName' => $request->input('categoryName'),
                'CategoryLink' => strtolower(str_replace("-"," ",$request->input('categoryName'))),
                'CategoryImage' => $url,
                'Parent' =>1,
                'Status' => $request->input('status'),
                'CreatedOn' => date('Y-m-d h:i:s a')
            ]);
            Session::flash('success', 'Successfully Created!'); 
        }
        return redirect('/categories');
    }

    public function delete(Request $request) {
        $request = Request::instance();
        $model = $request->input('table');
        if($model=='Category'){
        $data = Category::find($request->input('id'));
        }
        if($model=='Slider'){
            $data = Slider::find($request->input('id'));
        }
        if($model=='Product'){
            $data = Product::select("*")->where("ProductId", "=", $request->input('id'))->first();
            $var = ProductVariation::select("*")->where("ProductId", "=", $request->input('id'));
            $var->delete();
            $img = ProductImage::select("*")->where("ProductId", "=", $request->input('id'));
            $img->delete();
        }
        if($model=='Location'){
            $data = Location::find($request->input('id'));
        }
        $data->delete();
    }

    //Banner 

    public function banners() {
        $this->securePage();
        $data['data'] = Slider::all();
        return view('admin.banners')->with($data);
    }

    public function addBanner() {
        $data['data'] = '';
        return view('admin.bannerInfo')->with($data);
    }

    public function editBanner($id) {
        $data['data'] = Slider::find($id);
        return view('admin.bannerInfo')->with($data);
    }

    public function manageBanner(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('sliderId');
        if ($request->hasFile('image')) {
            //  Let's do everything here
            if ($request->file('image')->isValid()) {
                $validated = $request->validate([
                    'image' => 'mimes:jpeg,png|max:1014',
                ]);
                $extension = $request->image->extension();
                $fileName = 'banner'.time();
                $name = $fileName.'.'.$extension;
                $request->file('image')->move(public_path().'/assets/images', $name);  
                $url = 'public/assets/images/'.$name;
            }
        }
        if($id) {
            $post = Slider::find($id);
            if($url) {
                $post->Image = $url;
            }
            $post->Status = $request->input('status');
            $post->save();
            Session::flash('success', 'Successfully Updated!'); 
        } else {
            $file = Slider::create([
                'Image' => $url,
                'Status' => $request->input('status')
            ]);
            Session::flash('success', 'Successfully Created!'); 
        }
        return redirect('/banners');
    }

    //End of Banner


    //Product 

    public function products() {
        $this->securePage();
        $data['data']  = Product::select("*")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->join('categories','categories.CategoryId','=','products.Category')
        ->where("products.Status", "=", "A")
        ->groupBy('product_variations.ProductId')->get();

        return view('admin.products')->with($data);
    }

    public function addProduct() {
        $data['data'] = '';
        $data['categories'] = Category::all();
        return view('admin.productInfo')->with($data);
    }

    public function editProduct($id) {
        $data['data'] = Product::select("*")->where("ProductId", "=", $id)->first();
        $data['categories'] = Category::all();
        
        $data['options'] = ProductVariation::select("*")->where("ProductId", "=", $id)->get();
        $data['images'] = ProductImage::select("*")->where("ProductId", "=", $id)->get();
        return view('admin.productInfo')->with($data);
    }

    public function manageProduct(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('productId');
        
        if($id) {
            $user =  ProductVariation::select("*")->where("ProductId", "=", $id)->delete();
            $post = Product::select("*")->where("ProductId", "=", $id)->first();
            Session::flash('success', 'Successfully Updated!'); 
            $productId = $id;
        } else {
            $post = new Product;
            Session::flash('success', 'Successfully Created!'); 
        }
        $post->ProductName = $request->input('productName');
        $post->Category = $request->input('category');
        $post->ProductLink = strtolower(str_replace("-"," ",$request->input('productName')));
        $post->ProductDescription = $request->input('description');
        $post->Discount = $request->input('discount');
        $post->Stock = $request->input('stock');
        $post->Status = $request->input('status');
        $post->save();
       
        if(!$id) {
        $productId = $post->ProductId;
        }

        $options = $request->input('weight');
        $price = $request->input('price');
        $status = $request->input('option_status');

        for($i=0; $i<count($options); $i++)
        {
            $post1 = new ProductVariation;
            $post1->ProductId = $productId;
            $post1->Size = $options[$i];
            $post1->Price = $price[$i];
            $post1->Status = $status[$i];
            $post1->save();
        }

        if ($request->hasFile('image')) {

            if($id) {
                ProductImage::select("*")->where("ProductId", "=", $id)->delete();
            }
            
            foreach($request->file('image') as $file)
            {
                $rnd = rand(0,9999);
                $name = $rnd.'-'.str_replace(' ', '-', $request->input('productName')).'.'.$file->extension();
                $file->move(public_path().'/assets/images', $name);  
                $url = 'public/assets/images/'.$name;

                $file = ProductImage::create([
                    'ImagePath' => $url,
                    'ProductId' => $productId
                ]);

            }
        }

        return redirect('/products');
    }

    //End of Product

    //users 

    public function users() {
        $this->securePage();
        $data['data']  = User::all();
        return view('admin.users')->with($data);
    }

    // End of users

    public function getTotalItems($id) {
        $items= OrderDetail::select("*")
        ->where('OrderId','=',$id)
        ->get();
        echo count($items);
    }

    public function completeOrder($email, $id) {
        $order = Order::find($id);
        $order->Status = 'C';
        $order->save();

        $this->sendMail($email,$id);

        Session::flash('success', 'Order has been completed successfully..!');
        return redirect('/view-order/'.$id);
    }

    //orders 

    public function orders() {
        $this->securePage();
        $data['controller'] = $this;
        $data['data']  = Order::select("orders.*","users.UserName")
        ->join('users','users.UserId','=','orders.UserId')
        ->orderBy('OrderId','DESC')
        ->get();
        return view('admin.orders')->with($data);
    }

    // End of orders

    //location

    public function locations() {
        $this->securePage();
        $data['data'] = Location::all();
        return view('admin.locations')->with($data);
    }

    public function addLocation() {
        $data['data'] = '';
        return view('admin.locationInfo')->with($data);
    }

    public function editLocation($id) {
        $data['data'] = Location::find($id);
        return view('admin.locationInfo')->with($data);
    }

    public function manageLocation(Request $request) {
        $request = Request::instance();
        $url = '';
        $id = $request->input('locationId');

       

        if($id) {
            $post = Location::find($id);
            $post->name = $request->input('name');
            $post->pincode = $request->input('pincode');
            $post->Status = $request->input('status');
            $post->save();
            Session::flash('success', 'Successfully Updated!'); 
        } else {

            $exists = Location::where('name', $request->input('name'))->first();
            if($exists) {
                Session::flash('error', 'Location Name Already Exists..!');
                return redirect('/add-location');
            }
    
            $pincode_exists = Location::where('pincode', $request->input('pincode'))->first();
            if($pincode_exists) {
                Session::flash('error', 'Pincode Already Exists..!');
                return redirect('/add-location');
            }

            $file = Location::create([
                'name' => $request->input('name'),
                'pincode' => $request->input('pincode'),
                'status' => $request->input('status'),
                'created_at' => date('Y-m-d h:i:s a')
            ]);
            Session::flash('success', 'Successfully Created!'); 
        }
        return redirect('/locations');
    }

    //end of location

    //orders detail 

    public function viewOrder($id) {
        $this->securePage();
        $data['order'] = $order = Order::find($id);
        $data['customer']  = User::find($order->UserId);
        $data['shipping']  = ShippingDetail::where('ShippingId', $order->ShippingId)->first();
        $data['orders']  = OrderDetail::select("*")
        ->join('products','products.ProductId','=','order_details.ProductId')
        ->join('product_images','product_images.ProductId','=','order_details.ProductId')
        ->join('product_variations','product_variations.ProductId','=','order_details.ProductId')
        ->where('order_details.OrderId','=',$id)
        ->groupBy('product_images.ProductId')
        ->get();
        return view('admin.vieworder')->with($data);
    }

    public function printInvoice($id) {
        $this->securePage();
        $data['order'] = $order = Order::find($id);
        $data['customer']  = User::find($order->UserId);
        $data['shipping']  = ShippingDetail::where('ShippingId', $order->ShippingId)->first();
        $data['orders']  = OrderDetail::select("*")
        ->join('products','products.ProductId','=','order_details.ProductId')
        ->join('product_images','product_images.ProductId','=','order_details.ProductId')
        ->join('product_variations','product_variations.ProductId','=','order_details.ProductId')
        ->where('order_details.OrderId','=',$id)
        ->groupBy('product_images.ProductId')
        ->get();
        return view('admin.printInvoice')->with($data);
    }

    public function viewUser($id) {
        $this->securePage();
        $data['user']  = User::find($id);
        $data['controller'] = $this;
        $data['orders']  = Order::where('UserId', $id)->orderBy('OrderId','DESC')->get();
        return view('admin.view-user')->with($data);
    }

    public function sendMail($email,$order_id) {
        
        $items = OrderDetail::select("*")
        ->join('products','order_details.ProductId','=','products.ProductId')
        ->where("OrderId", "=", $order_id)->get();

        $order = Order::select("*")
        ->where("OrderId", "=", $order_id)->first();

        $shipping = ShippingDetail::select("*")
        ->where("OrderId", "=", $order_id)->first();

        $data = array('email'=>$email,'items'=>$items,'order'=>$order,'shipping'=>$shipping);
        Mail::send('mail.order', $data, function($message) use ($email) {
           $message->to($email)->subject
              ('Your Order has been delivered from Thatha FishShop');
           $message->from('info@thathafishshop.com','Thatha FishShop');
        });
        //echo "HTML Email Sent. Check your inbox.";
     }

    // End of orders detail

    public function securePage() {
        if(!Session::get('user_id')){
            return redirect('/');
        }
    }
}
