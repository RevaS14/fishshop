<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use App\Models\ProductVariation;
use App\Models\ProductImage;

class ProductController extends Controller
{
    public function index()
    {
        $data = Product::select("*")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->where("products.Status", "=", "A")
        ->groupBy('product_variations.ProductId')->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row; 
            $arr['variations'][]= $this->getVariations($row->ProductId);
        }
        return $arr;
        // return new CategoryCollection($arr);
    }

    public function category($link)
    {
        // $category = Category::select("*")
        // ->where("Status", "=", "A")
        // ->where("CategoryLink", "=", $link)
        // ->get();
        $data='';
        $data = Product::select("*")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->join('categories','categories.CategoryId','=','products.Category')
        ->where("products.Status", "=", "A")
        ->where("categories.CategoryLink", "=", $link)
        ->groupBy('product_variations.ProductId')->get();
        $arr = array();
        if(!$data->isEmpty()) {  
            foreach($data as $row){
            $arr['data'][] = $row; 
            $arr['variations'][]= $this->getVariations($row->ProductId);
        } } else {
            $arr['data']=''; 
            $arr['variations']='';
        }
        return $arr;
        // return new CategoryCollection($arr);
    }

    public function product($link)
    {

        $data='';
        $data = Product::select("*")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->join('categories','categories.CategoryId','=','products.Category')
        ->where("products.Status", "=", "A")
        ->where("products.ProductLink", "=", $link)
        ->groupBy('product_variations.ProductId')->get();
        $arr = array();
        if(!$data->isEmpty()) {  
            foreach($data as $row){
            $arr['data'][] = $row; 
            $arr['variations'][]= $this->getVariations($row->ProductId);
            $arr['images'][]= $this->getImages($row->ProductId);
        } } else {
            $arr['data']=''; 
            $arr['variations']='';
        }
        return $arr;
        // return new CategoryCollection($arr);
    }

    public function related($id)
    {
        $cat = Product::select("*")
        ->where("products.Status", "=", "A")
        ->where("products.ProductId", "=", $id)
        ->first();
        $arr = array();
        if($cat) {
        $data = Product::select("*")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->join('categories','categories.CategoryId','=','products.Category')
        ->where("products.Status", "=", "A")
        ->where("products.Category", "=", $cat->Category)
        ->where("products.ProductId", "!=", $id)
        ->groupBy('product_variations.ProductId')->get();
        
        if(!$data->isEmpty()) {  
            foreach($data as $row){
            $arr['data'][] = $row; 
            $arr['variations'][]= $this->getVariations($row->ProductId);
        } } else {
            $arr['data']=''; 
            $arr['variations']='';
        }
        }
        return $arr;
    }


    public function productImages($link)
    {

        $data='';
        $data = Product::select("*")
        ->join('product_images','product_images.ProductId','=','products.ProductId')
        ->join('product_variations','product_variations.ProductId','=','products.ProductId')
        ->join('categories','categories.CategoryId','=','products.Category')
        ->where("products.Status", "=", "A")
        ->where("products.ProductLink", "=", $link)
        ->groupBy('product_variations.ProductId')->get();
        $arr = array();
        $img = array();
        if(!$data->isEmpty()) {  
            foreach($data as $row){
            $arr = $this->getImages($row->ProductId);
            } 
            foreach($arr as $row1){
                $img[] = array('original'=>'http://dev.thathafishshop.com/'.$row1->ImagePath, 'thumbnail'=>'http://dev.thathafishshop.com/'.$row1->ImagePath);
            }  
            
            return $img;

    } else {
            $arr['data']=''; 
            $arr['variations']='';
        }
        //return $arr;
        // return new CategoryCollection($arr);
    }
    

    public function getVariations($productId)
    {
        return ProductVariation::select("*")
        ->where("ProductId", "=", $productId)->get();
    }
    public function getImages($productId)
    {
        return ProductImage::select("*")
        ->where("ProductId", "=", $productId)->get();
    }
}
