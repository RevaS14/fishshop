<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Slider;
use App\Http\Resources\Slider as SliderResource;
use App\Http\Resources\SliderCollection;

class SliderController extends Controller
{
    public function index()
    {
        return new SliderCollection(Slider::select("*")
        ->where("Status", "=", "A")->get());
    }
}
