<?php

namespace App\Http\Controllers;
use Request;
use App\Models\Category;
use App\Models\User;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use Hash;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::select("*")
        ->where("Status", "=", "A")->where("Parent", "=", NULL)->get();
        $arr = array();
        foreach($data as $row){
            $arr['data'][] = $row; 
            $arr['sub'][]= $this->getSub($row->CategoryId);
        }
        return $arr;
        // return new CategoryCollection($arr);
    }

    public function login(Request $request)
    {
        
        $request = Request::instance();
        $error = array(
            "Invalid"
        );
        $usererror = array(
            "Not found"
        );
        $user =  User::select("*")->where("Mobile", "=", $request->input('username'))->first();
        if($user){
            if (Hash::check($request->input('password'), $user->Password)) {
                return $user;
            } else {
                return $error;
            }
        } else {
            return $usererror;
        }
    }

    public function getSub($CategoryId)
    {
        return Category::select("*")
        ->where("Status", "=", "A")->where("Parent", "=", $CategoryId)->get();
    }

    public function getSubMenu($CategoryId)
    {
        return new CategoryCollection(Category::select("*")
        ->where("Status", "=", "A")->where("Parent", "=", $CategoryId)->get());
    }

    public function show($CategoryId)
    {
        return new CategoryResource(Category::findOrFail($CategoryId));
    }
}
