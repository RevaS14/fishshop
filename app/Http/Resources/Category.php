<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'CategoryId'     => $this->CategoryId,
            'CategoryName'   => $this->CategoryName,
            'CategoryLink'   => $this->CategoryLink,
            'CategoryImage'   => $this->CategoryImage,
            'Parent'    => $this->Parent,
            'Status'     => $this->Status,
            'CreatedOn'     => $this->CreatedOn,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
