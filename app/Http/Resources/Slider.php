<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Slider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'SliderId'     => $this->SliderId,
            'Image'   => $this->Image,
            'Status'     => $this->Status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
