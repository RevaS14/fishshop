<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('DetailId');
            $table->integer('UserId');
            $table->integer('OrderId');
            $table->integer('ProductId');
            $table->integer('ProductVariationId');
            $table->integer('Qty')->default(NULL);
            $table->string('Weight');
            $table->float('Price');
            $table->char('Status', 4)->default('P');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
