<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('OrderId');
            $table->integer('UserId');
            $table->integer('ShippingId');
            $table->float('Total');
            $table->float('ShippingCharge');
            $table->char('PaymentMethod', 4)->default('C');
            $table->string('Notes')->default('NULL');
            $table->string('Coupon')->default('NULL');
            $table->float('CouponPrice')->default(0);
            $table->char('Status', 4)->default('P');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
